﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using ActivityAnalysisLibrary.ML;
using CubeDataSample.Core;
using ActivityAnalysisLibrary.Core;
using ActivityAnalysisLibrary.Evaluation;
using System.IO;
using weka.core;
using weka.classifiers.trees;
using weka.classifiers;
using weka.filters.unsupervised.instance;
using weka.filters;
using weka.filters.unsupervised.attribute;

namespace EstimateUsersAttributes {
    class SophisticatedAnalysisTask : MultiThreadMethods.TaskClass {
        List<UserData> userlist;
        UserData testUser;//テストに使われるユーザ
        bool doTraining, doTest;//trainingをするかどうか，testをするかどうか
        Instances testInstances; // instances for testing
        Instances trainingInstances; // instances for training
        SophisticatedAnalysisSetting setting;

        public void SetParams(List<UserData> userList, UserData testUser, 
            Instances testInstances, Instances trainingInstances, bool doTraining, bool doTest, SophisticatedAnalysisSetting setting) {
            this.userlist = userList;
            this.testUser = testUser;
            this.testInstances = testInstances;
            this.trainingInstances = trainingInstances;
            this.doTraining = doTraining;
            this.doTest = doTest;
            this.setting = setting;
        }

        // 1ユーザをテストユーザとして、テストを行う
        public override void DoTask() {
            var classifierStr = setting.Classifier.ToString();
            string userDirPath = Path.Combine(setting.GetExportRootPath(), testUser.ID);
            Directory.CreateDirectory(userDirPath);
            var modelFileNameBuilder = new StringBuilder(testUser.ID).Append("_").Append(classifierStr).Append(".model");
            string modelFilePath = Path.Combine(userDirPath, modelFileNameBuilder.ToString());

            // リサンプルを行ってみる
            if (setting.UseResample) {
                var resampleFilter = new Resample();
                resampleFilter.setInputFormat(trainingInstances);
                trainingInstances = Filter.useFilter(trainingInstances, resampleFilter);
            }
            // 主成分分析フィルタ
            if (setting.UsePCA) {
                var pcaFilter = new PrincipalComponents();
                pcaFilter.setVarianceCovered(0.5);
                pcaFilter.setInputFormat(trainingInstances);
                trainingInstances = Filter.useFilter(trainingInstances, pcaFilter);
                testInstances = Filter.useFilter(testInstances, pcaFilter);
            }
            weka.classifiers.Classifier classifier = null;
            var recorder = new TimeRecorder();//時間計測用のクラスをセットする
            //モデルの学習&保存
            if (doTraining || File.Exists(modelFilePath) == false)
                classifier = WekaUsefulMethods.BuildClassifier(trainingInstances, setting.Classifier, null, true, modelFilePath, recorder);
            //学習済モデルのロード
            if (classifier == null)
                classifier = WekaUsefulMethods.LoadSavedClassifier(modelFilePath, setting.Classifier, null);

            if (doTest)//テスト
            {
                // クラスに所属する確率を格納した配列
                var classProbabilityList = new List<double[]>();
                foreach (int instanceIdx in Enumerable.Range(0, testInstances.numInstances())) {
                    classProbabilityList.Add(classifier.distributionForInstance(testInstances.instance(instanceIdx)));
                }
                if (classProbabilityList != null && classProbabilityList.Count > 0) {
                    var eval = new Evaluation(trainingInstances);
                    // 10 fold cross validation 評価を行う
                    //eval.crossValidateModel(classifier, trainingInstances, 10, new java.util.Random(1));
                    eval.evaluateModel(classifier, testInstances);
                    string finalAnswer;
                    if (Utility.IsNumericalPhysicalChar(setting.PhysicalChar))
                        finalAnswer = classProbabilityList.Select(darray => darray[0]).Average().ToString();
                    else{
                        var classValues = Utility.GetNominalStringClass(setting.PhysicalChar);
                        var classCountDict = new Dictionary<string, int>();
                        classProbabilityList.Select(darray =>
                            // 各classProbability配列のうち、最大の値を持つものを選択し、そのクラスの値を求める
                            classValues[darray.Select((val, idx) => new { V = val, I = idx }).Aggregate((selected, working) => selected.V > working.V ? selected : working).I]
                            ).ToList().ForEach(classVal => classCountDict[classVal] = classCountDict.ContainsKey(classVal) ?  classCountDict[classVal] + 1 : 1);
                        // classCountDictには、分類されたクラスの数が格納されているため、その中で最も多く分類されたクラスをfinalAnswerとする
                        finalAnswer = classCountDict.Aggregate((selectedKV, workingKV) => selectedKV.Value > workingKV.Value ? selectedKV : workingKV).Key;
                    }
                    using (var sw = new StreamWriter(Path.Combine(userDirPath, testUser.ID + ".csv"))) {
                        sw.WriteLine(finalAnswer);
                        sw.WriteLine();
                        sw.WriteLine(eval.toSummaryString());
                        sw.WriteLine();
                        sw.WriteLine("classProbabilityList");
                        classProbabilityList.ForEach(darray => sw.WriteLine(string.Join(",", darray)));
                    }
                }
            }
            recorder.OutResults(Path.Combine(userDirPath, "timerecord.txt"));
        }
    }
}

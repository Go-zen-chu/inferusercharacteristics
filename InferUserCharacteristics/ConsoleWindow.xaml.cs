﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using System.ComponentModel;

namespace EstimateUsersAttributes
{
    public enum ErrorStatus
    {
        OK, Warning, Error
    }

    /// <summary>
    /// ConsoleWindow.xaml の相互作用ロジック
    /// </summary>
    public partial class ConsoleWindow : Window
    {
        public ConsoleWindow()
        {
            InitializeComponent();
        }

        // UIスレッドや他のスレッドから呼んでも大丈夫
        public void Print(string message, ErrorStatus status = ErrorStatus.OK, params object[] objs){
            if(objs != null){
                for(int i = 0; i < objs.Length; i++){
                    message = message.Replace("{" + i + "}", objs[i].ToString());
                }
            }
            addConsoleMessage(new ConsoleMessage(message, status));
        }

        void addConsoleMessage(ConsoleMessage consoleMessage)
        {
            if (consoleMessage == null) return;
            // 他のスレッドでも大丈夫なようにinvoke
            Dispatcher.BeginInvoke(new Action(() =>
            {
                var grid = new Grid();
                for (int i = 0; i < 2; i++) grid.RowDefinitions.Add(new RowDefinition());
                var timeText = new TextBlock();
                timeText.Text = consoleMessage.Time;
                timeText.Foreground = Brushes.Blue;
                var newMessage = new TextBlock();
                newMessage.Text = consoleMessage.Message;
                switch (consoleMessage.ErrorStatus)
                {
                    case ErrorStatus.OK: newMessage.Foreground = Brushes.Green; break;
                    case ErrorStatus.Warning: newMessage.Foreground = Brushes.Yellow; break;
                    case ErrorStatus.Error: newMessage.Foreground = Brushes.Red; break;
                }
                newMessage.SetValue(Grid.RowProperty, 1);
                grid.Children.Add(timeText);
                grid.Children.Add(newMessage);
                mainListBox.Items.Add(grid);
                mainListBox.ScrollIntoView(mainListBox.Items[mainListBox.Items.Count - 1]);
            }));
        }

        public class ConsoleMessage
        {
            public string Time;
            public string Message;
            public ErrorStatus ErrorStatus;
            public ConsoleMessage(string message, ErrorStatus status)
            {
                this.Time = DateTime.Now.ToString();
                this.Message = message;
                this.ErrorStatus = status;
            }
        }

    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using ActivityAnalysisLibrary.Core;
using System.Windows.Forms;
using weka.core;
using ActivityAnalysisLibrary.ML;
using System.Text.RegularExpressions;
using System.IO;

namespace EstimateUsersAttributes {
    static class Utility {
        // 各ユーザがもつ9つのARFFファイルを何個読むか（多いほうが正確だが、時間がかかる）
        public static readonly int READ_ARFF_NUMBER = 1;
        public static readonly Dictionary<string, WekaConstantParams.WekaClassifiers> NominalMethodsDict = new Dictionary<string, WekaConstantParams.WekaClassifiers>() {
            { "J48", WekaConstantParams.WekaClassifiers.J48},
            { "SMO", WekaConstantParams.WekaClassifiers.SMO},
            { "NaiveBayes", WekaConstantParams.WekaClassifiers.NaiveBayes},
            //{ "Logistic", WekaConstantParams.WekaClassifiers.Logistic},
            //{ "AdaBoost", WekaConstantParams.WekaClassifiers.AdaBoost},
            { "RandomForest", WekaConstantParams.WekaClassifiers.RandomForest},
            { "IB3", WekaConstantParams.WekaClassifiers.IBk}
        };
        public static readonly Dictionary<string, WekaConstantParams.WekaClassifiers> NumericMethodsDict = new Dictionary<string, WekaConstantParams.WekaClassifiers>() {
            { "SMOreg", WekaConstantParams.WekaClassifiers.SMOreg},
            { "LinearRegression", WekaConstantParams.WekaClassifiers.LinearRegression},
            //{ "LeastMedSq", WekaConstantParams.WekaClassifiers.LeastMedSq},
            { "MultilayerPerception", WekaConstantParams.WekaClassifiers.MultilayerPerception},
            { "IB3", WekaConstantParams.WekaClassifiers.IBk}
        };

        public static readonly List<int> AllSensorPatternIndices = new List<int>() { 1, 2, 4, 8, 3, 12, 15 };
        public static void Swap<T>(ref T object1, ref T object2) {
            var tmp = object1;
            object1 = object2;
            object2 = tmp;
        }
        public static int[] ConvertStrArrayToIntArray(string[] stringArray) {
            int convertedNum;
            var convertedArray = new int[stringArray.Length];
            for (int i = 0; i < stringArray.Length; i++) {
                if (int.TryParse(stringArray[i], out convertedNum)) convertedArray[i] = convertedNum;
                else convertedArray[i] = -1;
            }
            return convertedArray;
        }

        #region select folder or dirs
        public static string SelectFolderPath(string rootPath = null, string description = "Choose Folder") {
            var folderBrowserDialog = new FolderBrowserDialog();
            folderBrowserDialog.SelectedPath = rootPath;
            folderBrowserDialog.Description = description;
            var dialogResult = folderBrowserDialog.ShowDialog();
            if (dialogResult == System.Windows.Forms.DialogResult.OK) {
                return folderBrowserDialog.SelectedPath;
            }
            return null;
        }

        public static string SelectFilePath() {
            var openFileDialog = new OpenFileDialog();
            openFileDialog.Title = "Choose File";
            var dialogResult = openFileDialog.ShowDialog();
            if (dialogResult == System.Windows.Forms.DialogResult.OK) {
                return openFileDialog.FileName;
            }
            return null;
        }

        public static string[] SelectMultipleFilePath() {
            var openFileDialog = new OpenFileDialog();
            openFileDialog.Title = "Choose Files";
            openFileDialog.Multiselect = true;
            var dialogResult = openFileDialog.ShowDialog();
            if (dialogResult == System.Windows.Forms.DialogResult.OK) {
                return openFileDialog.FileNames;
            }
            return null;
        }
        #endregion

        #region physical char related methods

        public static bool IsNumericalPhysicalChar(PhysicalChar physicalChar) {
            switch (physicalChar) {
                case PhysicalChar.age:
                case PhysicalChar.height:
                case PhysicalChar.weight:
                case PhysicalChar.pingpongLevel:
                case PhysicalChar.pingpongExperienceNumeric:
                    return true;
                default:
                    return false;
            }
        }
        // 不使用
        public static FastVector CreatePhysicalCharClassVector(PhysicalChar physicalChar) {
            if (IsNumericalPhysicalChar(physicalChar)) return null;
            FastVector physicalCharClassVector = null;
            switch (physicalChar) {
                #region fastvectorでどのようなクラスがあるかを定義
                case PhysicalChar.gender: {
                        physicalCharClassVector = new FastVector(2);
                        physicalCharClassVector.addElement("Male");
                        physicalCharClassVector.addElement("Female");
                    } break;
                case PhysicalChar.pingpongDominant:
                case PhysicalChar.writingDominant: {
                        physicalCharClassVector = new FastVector(2);
                        physicalCharClassVector.addElement("Right");
                        physicalCharClassVector.addElement("Left");
                    } break;
                case PhysicalChar.vacuumingDominant: {
                        physicalCharClassVector = new FastVector(3);
                        physicalCharClassVector.addElement("Right");
                        physicalCharClassVector.addElement("Both");
                        physicalCharClassVector.addElement("Left");
                    } break;
                case PhysicalChar.bikeType: {
                        physicalCharClassVector = new FastVector(2);
                        physicalCharClassVector.addElement("Normal");
                        physicalCharClassVector.addElement("MTB");
                    } break;
                case PhysicalChar.touchTyping: {
                        physicalCharClassVector = new FastVector(3);
                        physicalCharClassVector.addElement("Yes");
                        physicalCharClassVector.addElement("Middle");
                        physicalCharClassVector.addElement("No");
                    } break;
                case PhysicalChar.washDishFreq: {
                        physicalCharClassVector = new FastVector(4);
                        physicalCharClassVector.addElement("Everyday");
                        physicalCharClassVector.addElement("Usually");
                        physicalCharClassVector.addElement("AlmostNever");
                        physicalCharClassVector.addElement("Never");
                    } break;
                case PhysicalChar.devidedHeight: {
                        physicalCharClassVector = new FastVector(6);
                        foreach (var physicalCharObj in Enum.GetValues(typeof(DevidedHeight)))
                            physicalCharClassVector.addElement(physicalCharObj.ToString());
                    } break;
                #endregion
            }
            return physicalCharClassVector;
        }

        public static List<string> GetNominalStringClass(PhysicalChar physicalChar) {
            List<string> classList = null;
            switch (physicalChar) {
                case PhysicalChar.gender: {
                        classList = new List<string>() { "Male", "Female" };
                    } break;
                case PhysicalChar.pingpongDominant:
                case PhysicalChar.writingDominant: {
                        classList = new List<string>() { "Right", "Left" };
                    } break;
                case PhysicalChar.brushingDominant:
                case PhysicalChar.vacuumingDominant: {
                        classList = new List<string>() { "Right", "Both", "Left" };
                    } break;
                case PhysicalChar.bikeType: {
                        classList = new List<string>() { "Normal", "MTB" };
                    } break;
                case PhysicalChar.chairType: {
                        classList = new List<string>() { "Chair", "Sofa" };
                    } break;
                case PhysicalChar.pingpoingExperience:
                case PhysicalChar.touchTyping: {
                        classList = new List<string>() { "Yes", "Middle", "No" };
                    } break;
                case PhysicalChar.bikeFrequency:
                case PhysicalChar.washDishFreq: {
                        classList = new List<string>() { "Everyday", "Usually", "AlmostNever", "Never" };
                    } break;
                case PhysicalChar.vacuumType: {
                        classList = new List<string>() { "Normal", "Handy" };
                    }break;
                case PhysicalChar.devidedHeight: {
                        classList = Enum.GetValues(typeof(DevidedHeight)).OfType<DevidedHeight>().Select(cls => cls.ToString()).ToList();
                    } break;
                default: throw new Exception(string.Format("Not surported trait {0}", physicalChar));
            }
            return classList;
        }

        public static string GetNominalPhysicalCharData(UserData user, PhysicalChar physicalChar) {
            switch (physicalChar) {
                case PhysicalChar.gender: return user.nominalattributes[2].value;
                case PhysicalChar.pingpongDominant: return user.nominalattributes[3].value;
                case PhysicalChar.writingDominant: return user.nominalattributes[4].value;
                case PhysicalChar.brushingDominant: return user.nominalattributes[5].value;
                case PhysicalChar.vacuumingDominant: return user.nominalattributes[6].value;
                case PhysicalChar.bikeType: return user.nominalattributes[7].value;
                case PhysicalChar.chairType: return user.nominalattributes[8].value;
                case PhysicalChar.pingpoingExperience: return user.nominalattributes[9].value;
                case PhysicalChar.touchTyping: return user.nominalattributes[10].value;
                case PhysicalChar.washDishFreq: return user.nominalattributes[11].value;
                case PhysicalChar.bikeFrequency: return user.nominalattributes[13].value;
                case PhysicalChar.vacuumType: return user.nominalattributes[14].value;
                case PhysicalChar.devidedHeight: return GetDevidedHeightClass(user.numericattributes[1].value).ToString();
            }
            throw new Exception(string.Format("PhysicalChar {0} is not nominal", physicalChar));
        }

        public static DevidedHeight GetDevidedHeightClass(double height) {
            double minHeight = 150;
            if (minHeight <= height && height < minHeight + 6) return DevidedHeight.A;
            else if (minHeight + 6 <= height && height < minHeight + 11) return DevidedHeight.B;
            else if (minHeight + 11 <= height && height < minHeight + 16) return DevidedHeight.C;
            else if (minHeight + 16 <= height && height < minHeight + 21) return DevidedHeight.D;
            else if (minHeight + 21 <= height && height < minHeight + 26) return DevidedHeight.E;
            else return DevidedHeight.F;
        }

        public static double GetAverageHeightOfClass(DevidedHeight devidedHeight) {
            switch (devidedHeight) {
                case DevidedHeight.A: return 153;
                case DevidedHeight.B: return 158.5;
                case DevidedHeight.C: return 163.5;
                case DevidedHeight.D: return 168.5;
                case DevidedHeight.E: return 173.5;
                case DevidedHeight.F: return 179;
                default: return 0;
            }
        }

        public static double GetNumericalPhysicalCharData(UserData user, PhysicalChar physicalChar) {
            switch (physicalChar) {
                case PhysicalChar.age: return user.numericattributes[0].value;
                case PhysicalChar.height: return user.numericattributes[1].value;
                case PhysicalChar.weight: return user.numericattributes[2].value;
                case PhysicalChar.pingpongLevel: return user.numericattributes[3].value;
                case PhysicalChar.pingpongExperienceNumeric:
                    switch (user.nominalattributes[9].value) {
                        case "Yes": return 2;
                        case "Middle": return 1;
                        case "No": return 0;
                        default: throw new Exception("Something wrong with nominal classes");
                    }
            }
            throw new Exception(string.Format("PhysicalChar {0} is not numerical", physicalChar));
        }

        public static string GetPhysicalCharDataString(UserData user, PhysicalChar physicalChar) {
            if (IsNumericalPhysicalChar(physicalChar)) {
                return GetNumericalPhysicalCharData(user, physicalChar).ToString();
            } else {
                return GetNominalPhysicalCharData(user, physicalChar);
            }
        }

        public static PhysicalChar GetPhysicalCharFromString(string physicalCharStr) {
            foreach (var physchar in Enum.GetValues(typeof(PhysicalChar)) as PhysicalChar[]) {
                if (physchar.ToString() == physicalCharStr) return physchar;
            }
            throw new Exception("Doesn't match to any of physical char");
        }

        public static List<PhysicalChar> GetNumericalPhysicalCharList() {
            var list = new List<PhysicalChar>();
            foreach (var physChar in Enum.GetValues(typeof(PhysicalChar)) as PhysicalChar[]) {
                if (IsNumericalPhysicalChar(physChar)) {
                    list.Add(physChar);
                }
            }
            return list;
        }

        public static List<PhysicalChar> GetNominalPhysicalCharList() {
            return (Enum.GetValues(typeof(PhysicalChar)) as PhysicalChar[]).Except(GetNumericalPhysicalCharList()).ToList();
        }

        public static List<double> GetPhysicalCharBorders(PhysicalChar physicalChar) {
            if (IsNumericalPhysicalChar(physicalChar)) {
                switch (physicalChar) {
                        // 境界値を返す。ageの場合、32才以下、33 - 45才、46才以上
                    case PhysicalChar.age: return new List<double>() { 32, 45, 58 };
                    case PhysicalChar.height: return new List<double>() { 161, 171, 182 };
                    case PhysicalChar.weight: return new List<double>() { 51, 63, 90 };
                    case PhysicalChar.pingpongLevel: return new List<double>() { 1, 2, 3, 4 };
                    case PhysicalChar.pingpongExperienceNumeric: return new List<double>() { 0, 1, 2 };
                    default: throw new NotImplementedException();
                }
            } else
                throw new Exception("physical char has to be numerical");
        }
        #endregion

        #region weka related methods

        // 一次元配列をinstanceに変換する
        public static Instances MakeInstances(double[] doubleVector) {
            if (doubleVector == null || doubleVector.Length == 0) return null;
            var attribute = new weka.core.Attribute("doubleValue");
            var relationVector = new FastVector(1);
            relationVector.addElement(attribute);
            var instances = new Instances("relation", relationVector, 1);
            foreach (var value in doubleVector) {
                var instance = new Instance(1);
                instance.setValue(instances.attribute("doubleValue"), value);
                instances.add(instance);
            }
            return instances;
        }

        /// <summary>
        /// 正規表現で指定したinstancesのattributeを削除する
        /// </summary>
        /// <param name="instances"></param>
        /// <param name="regexpStr"></param>
        /// <returns></returns>
        public static Instances ExtractAttributesByRegexp(Instances instances, string regexpStr) {
            var noNeedAttributes = new List<string>();
            var regexp = new Regex(regexpStr);
            for (int attributeIndex = 0; attributeIndex < instances.numAttributes(); attributeIndex++) {
                // クラスインデックスは削除しないようにする
                if (attributeIndex == instances.classIndex()) continue;
                var attributeName = instances.attribute(attributeIndex).name();
                if (regexp.IsMatch(attributeName) == false) noNeedAttributes.Add(attributeName);
            }
            WekaUsefulMethods.ReduceAttributes(instances, noNeedAttributes.ToArray());
            return instances;
        }

        #region Arff関係
        //ARFFファイルの読み込みとInstancesの生成(instancesDictに値を代入する)
        public static void LoadOriginalArffFiles(string arffFilesPath, List<UserData> selectedUsers, ref Dictionary<UserData, Instances> originalInstancesDict) {
            foreach (var tpl in selectedUsers.Select((u, i) => new { user = u, idx = i })) {
                if (originalInstancesDict.ContainsKey(tpl.user)) continue;
                var instances = ConvertArffToInstances(Path.Combine(arffFilesPath, tpl.user.ID), READ_ARFF_NUMBER);
                if (instances == null) throw new Exception("Couldn't make instances! Check process Folder and check arff dir!");
                originalInstancesDict.Add(tpl.user, instances);
                MainWindow.CW.Print("Loading Arff file of " + tpl.user.ID + "  " + (tpl.idx + 1) + "/" + selectedUsers.Count);
            }
        }

        public static void LoadRecognisedArffFiles(string recognisedArffRootPath, List<UserData> selectedUsers, ref Dictionary<UserData, Instances> recognisedInstancesDict) {
            foreach (var tpl in selectedUsers.Select((u, i) => new { user = u, idx = i })) {
                if (recognisedInstancesDict.ContainsKey(tpl.user)) continue;
                var instances = ConvertArffToInstances(Path.Combine(recognisedArffRootPath, tpl.user.ID, CalcSimilarityOfUsers.RECOGNISED_ARFF), Utility.READ_ARFF_NUMBER);
                if (instances == null) throw new Exception("Couldn't make instances! Check process Folder and check arff dir!");
                recognisedInstancesDict.Add(tpl.user, instances);
                MainWindow.CW.Print("Loading Recognised Arff file of " + tpl.user.ID + "  " + (tpl.idx + 1) + "/" + selectedUsers.Count);
            }
        }

        //指定したユーザーのArffファイルをInstancesに変換する(arffNum => 読み込むファイルの数)
        public static weka.core.Instances ConvertArffToInstances(string arffParentPath, int readArffNum) {
            var arffFilePaths = new List<string>();
            int readedArffFileNum = 0;
            // ユーザーの全Arffファイルを取得する
            foreach (var arffPath in Directory.GetFiles(arffParentPath, "*.arff")) {
                arffFilePaths.Add(arffPath);
                readedArffFileNum++;
                if (readedArffFileNum == readArffNum) break;
            }
            if (readedArffFileNum == 0) {
                throw new Exception("ユーザーのARFFファイルが存在しません。processフォルダにarffファイルが存在するようにしてください");
            }
            // Arffファイルを選択された全ユーザーから読み込んだ
            var instances = WekaUsefulMethods.LoadArffFiles(arffFilePaths);
            instances.setClassIndex(instances.numAttributes() - 1);
            return instances;
        }

        // 行動、センサの組み合わせで、各ユーザーのインスタンスから、特定のデータを抽出する
        public static Dictionary<UserData, Instances> ExtractByActivityAndSensors(Dictionary<UserData, Instances> instancesDict, List<UserData> selectedUsers, Activity activity, bool[] usedSensors) {
            var extractedDict = new Dictionary<UserData, Instances>();
            foreach (var user in selectedUsers) {
                // activity_comboBoxで選択したクラス（行動）でインスタンスを抽出する
                var extractedInstances = instancesDict[user];
                if(activity != Activity.AllActivities)
                    extractedInstances = WekaUsefulMethods.ExtractInstancesByClass(instancesDict[user], activity.ToString());
                // クラスターを作るときに、classアトリビュートを削除しておかなければならない
                extractedInstances.setClassIndex(-1);
                WekaUsefulMethods.ReduceAttribute(extractedInstances, "class");
                var regexpStr = ".+_[";
                // 1->left hand, 2->right hand, 3->waist, 4->thigh
                for (int sensorID = 1; sensorID <= 4; sensorID++)
                    if (usedSensors[sensorID - 1]) regexpStr += sensorID.ToString();
                regexpStr += "]_.+";
                extractedInstances = Utility.ExtractAttributesByRegexp(extractedInstances, regexpStr);
                extractedDict.Add(user, extractedInstances);
            }
            return extractedDict;
        }

        #endregion

        #endregion

        #region sensor related methods
        // たとえばflagsInTwoBit=10だったら、1010に変換し、True,False,True,Falseの配列を返す
        public static bool[] MakeFlags(int flagsInTwoBit, int flagNum = 4) {
            if (flagsInTwoBit < 0 || flagNum <= 0) {
                return null;
            } else {
                var resultFlags = new bool[flagNum];
                // 各桁が1になっているかどうかを << と & で判断させる
                for (int position = 0; position < flagNum; position++) {
                    var digitNum = (1 << flagNum - position - 1);
                    resultFlags[position] = ((flagsInTwoBit & digitNum) == digitNum) ? true : false;
                }
                return resultFlags;
            }
        }
        // チェックしたセンサに応じて、1か0かのフラグ文字列を作成
        public static string UsedSensorString(bool[] usedSensors) {
            if (usedSensors == null || usedSensors.Length == 0) return null;
            var resultString = new List<int>();
            foreach (var b in usedSensors) resultString.Add(b ? 1 : 0);
            return string.Join("", resultString);
        }
        // sensorTypeStr(e.g 0010, 1001)で用いられたセンサが　usedSensorでも使われているか
        public static bool ContainsSensorType(bool[] usedSensor, string sensorTypeStr) {
            if (sensorTypeStr.Length != usedSensor.Length) throw new Exception("length of usedSensor and sensorTypeStr are different !");
            for (int i = 0; i < usedSensor.Length; i++)
                // sensorType では用いられているが、usedSensorでは用いられていない場合、ダメ
                if (sensorTypeStr[i] == '1' && (usedSensor[i] == false)) return false;
            return true;
        }

        public static List<string> GetContainedSensorTypes(bool[] usedSensor) {
            var preSensorTypes = new List<string>() { "" };
            foreach (var flg in usedSensor) {
                var postSensorTypes = new List<string>();
                foreach (var preSensorType in preSensorTypes) {
                    postSensorTypes.Add(preSensorType + "0");
                    if (flg) postSensorTypes.Add(preSensorType + "1");
                }
                preSensorTypes = postSensorTypes;
            }
            // 0000は不必要なので削除
            preSensorTypes.Remove(new string('0', usedSensor.Length));
            return preSensorTypes;
        }

        #endregion

        public static WekaConstantParams.WekaClassifiers GetClassifier(string selectedMethod, bool isNominal) {
            return isNominal ? NominalMethodsDict[selectedMethod] : NumericMethodsDict[selectedMethod];
        }

    }

    public enum PhysicalChar {
        //Numerical
        age, height, weight, pingpongLevel, pingpongExperienceNumeric,
        // Nominal
        gender, pingpongDominant, writingDominant, brushingDominant, vacuumingDominant,
        bikeType, chairType, pingpoingExperience, touchTyping, washDishFreq, bikeFrequency, vacuumType,
        devidedHeight
    }
    public enum DevidedHeight {
        // A: 150-156, B: 156-161, C:161-166, D 166-171, E:171-176, F176-182
        A, B, C, D, E, F
    }

    public enum Activity {
        AllActivities, Standing, Walking, Running, Sitting, StairsUp, StairsDown, Bicycling,
        BrushingTeeth, WashingDishes, UsingPC, DrawingWhiteboard, WritingNotebook, PlayPingpong, Vacuuming
    }
}

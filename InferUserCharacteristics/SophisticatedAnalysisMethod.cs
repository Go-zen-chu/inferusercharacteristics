﻿using System;
using System.Collections.Generic;
using ActivityAnalysisLibrary.ML;
using ActivityAnalysisLibrary.Core;
using weka.core;
using CubeDataSample.Core;
using MultiThreadMethods;
using System.Linq;
using System.IO;
using System.Collections.Concurrent;

namespace EstimateUsersAttributes {
    public class SophisticatedAnalysisMethod : WekaLearnMethod {
        SophisticatedAnalysisSetting setting;
        List<UserData> userList;
        public Dictionary<UserData, Tuple<Instances, List<Instances>>> TestAndTrainingInstancesDict { get; set; }
        public Dictionary<UserData, Instances> AllInstancesDict { get; set; }
        public ConcurrentDictionary<UserData, Instances> AllInstancesConcurrentDict { get; set; }
        public static string NOMINAL_RESULT_PATH = "errata.csv";
        public static string NUMERICAL_RESULT_PATH = "averageErrorDiff.csv";


        public SophisticatedAnalysisMethod(MainManager mman, DataManager dman, List<UserData> userList, 
            SophisticatedAnalysisSetting setting)
            : base(mman, dman) {
            // 並列処理時に、ユーザリストへの多重アクセスが存在する
            this.userList = new List<UserData>(userList);
            this.setting = setting;
        }

        protected override List<TaskClass> MakeTrainAndTestTasks(bool doTraining, bool doTest) {
            var tasks = new List<TaskClass>();
            foreach (var testUser in userList) {
                var task = new SophisticatedAnalysisTask();
                Instances testInstances;
                Instances trainingInstances;
                if (TestAndTrainingInstancesDict != null) {
                    testInstances = new Instances(TestAndTrainingInstancesDict[testUser].Item1);
                    trainingInstances = WekaUsefulMethods.CombInstances(TestAndTrainingInstancesDict[testUser].Item2);
                } else if (AllInstancesDict != null || AllInstancesConcurrentDict != null) {
                    var dict =  AllInstancesDict != null ? AllInstancesDict as IDictionary<UserData, Instances> : AllInstancesConcurrentDict as IDictionary<UserData, Instances>;
                    testInstances = new Instances(dict[testUser]);
                    // new Instancesにしなければ、allInstancesDictが上書きされてしまう
                    trainingInstances = WekaUsefulMethods.CombInstances( (from kv in dict where kv.Key != testUser select new Instances(kv.Value)).ToList());
                }else{
                    throw new Exception("Set Instances by using SetInstances() Method");
                }
                //ユーザごとにタスクを作る（一人のユーザを除くクロスバリデーションのため）
                task.SetParams(userList, testUser, testInstances, trainingInstances, doTraining, doTest, setting);
                tasks.Add(task);
            }
            return tasks;
        }

        protected override void Evaluate()//結果を評価
        {
            userList.Sort(new UserDataComparer());
            var userNum = userList.Count;

            if (Utility.IsNumericalPhysicalChar(setting.PhysicalChar)) {
                #region 連続な値に対する評価
                var answerArray = new double[userNum];
                var inferredValues = new double[userNum];
                var absError = new double[userNum];
                var squareError = new double[userNum];
                // グループごとにどれほど絶対誤差が異なるのかを調べる
                var borders = Utility.GetPhysicalCharBorders(setting.PhysicalChar).Select(border => new Tuple<double, double, int>(border, 0, 0)).ToList();

                foreach (var tpl in userList.Select((v, i) => new { User = v, Idx = i })) {
                    var userDirPath = Path.Combine(setting.GetExportRootPath(), tpl.User.ID);
                    var csvFilePath = Path.Combine(userDirPath, tpl.User.ID + ".csv");
                    string resultString = null;
                    // 予測した値が入っている最初の一行だけ読み取る。
                    using (var sr = new StreamReader(csvFilePath)) resultString = sr.ReadLine();
                    if (resultString != null) {
                        double inferredValue;
                        if (Double.TryParse(resultString, out inferredValue)) {
                            var answer = Utility.GetNumericalPhysicalCharData(tpl.User, setting.PhysicalChar);
                            answerArray[tpl.Idx] = answer;
                            inferredValues[tpl.Idx] = inferredValue;
                            absError[tpl.Idx] = Math.Abs(inferredValue - answer);
                            squareError[tpl.Idx] = Math.Pow(inferredValue - answer, 2);
                            for (int bdrIdx = 0; bdrIdx < borders.Count; bdrIdx++) {
                                var borderVal = borders[bdrIdx].Item1;
                                if (answer <= borderVal) {
                                    // item1 は境界値で不変、item2は絶対誤差の総和、item3はitem1の境界値に含まれる数
                                    borders[bdrIdx] = new Tuple<double, double, int>(borderVal, borders[bdrIdx].Item2 + absError[tpl.Idx], borders[bdrIdx].Item3 + 1); 
                                    break;
                                }
                            }
                        }
                    }
                }
                if (absError != null)
                    using (var sw = new StreamWriter(Path.Combine(setting.GetExportRootPath(), "averageErrorDiff.csv"))) {
                        sw.WriteLine(string.Join(",", answerArray));
                        sw.WriteLine(string.Join(",", inferredValues));
                        sw.WriteLine(string.Join(",", absError));
                        sw.WriteLine("MeanAbsoluteError, " + absError.Average());
                        sw.WriteLine(string.Join(",", squareError));
                        sw.WriteLine("RootMeanSquareError, " + Math.Sqrt(squareError.Average()));
                        sw.WriteLine("Borders, " + string.Join(",", borders.Select(b => b.Item1)));
                        sw.WriteLine("BorderMeanAbsError, " + string.Join(",", borders.Select(b => b.Item3 != 0 ? b.Item2 / b.Item3 : 0)));
                    }

                #endregion
            } else {
                #region 離散的値に対する評価
                var answerArray = new string[userNum];
                var inferredValues = new string[userNum];
                var errata = new bool[userNum];
                var countDict = new Dictionary<string, Tuple<int, int>>();

                foreach (var tpl in userList.Select((v, i) => new { User = v, Idx = i })) {
                    var userDirPath = Path.Combine(setting.GetExportRootPath(), tpl.User.ID);
                    var csvFilePath = Path.Combine(userDirPath, tpl.User.ID + ".csv");
                    string result = null;
                    using (var sr = new StreamReader(csvFilePath)) result = sr.ReadLine();
                    if (result != null) {
                        var answer = Utility.GetNominalPhysicalCharData(tpl.User, setting.PhysicalChar);
                        answerArray[tpl.Idx] = answer;
                        inferredValues[tpl.Idx] = result;
                        var isCorrect = inferredValues[tpl.Idx] == answer;
                        errata[tpl.Idx] = isCorrect;
                        var addedValue = isCorrect ? 1 : 0;
                        countDict[answer] = (countDict.ContainsKey(answer)) ? new Tuple<int, int>(countDict[answer].Item1 + addedValue, countDict[answer].Item2 + 1) : new Tuple<int, int>(addedValue, 1); 
                    }
                }

                using (var sw = new StreamWriter(Path.Combine(setting.GetExportRootPath(), "errata.csv"))) {
                    sw.WriteLine(string.Join(",", answerArray));
                    sw.WriteLine(string.Join(",", inferredValues));
                    sw.WriteLine(string.Join(",", errata));
                    sw.WriteLine((errata.Count(b => b) / (double)userNum) * 100);
                    // 属性ごとの正答率を出力
                    sw.WriteLine(string.Join(", ", countDict.ToList().Select(kv => string.Format("{0} {1}/{2} {3}", kv.Key, kv.Value.Item1, kv.Value.Item2, ((double)kv.Value.Item1) / kv.Value.Item2))));
                }
                #endregion
            }
        }
    }

    // 値型にするため、構造体
    public struct SophisticatedAnalysisSetting {
        // you have to fill all of these 
        public string WorkingDir { get; set; }
        public bool IsNormalMethod { get; set; }
        public bool IsLabeled { get; set; }
        public PhysicalChar PhysicalChar { get; set; }
        public WekaConstantParams.WekaClassifiers Classifier { get; set; }
        public bool[] UsedSensors { get; set; }
        public Activity Activity { get; set; }
        public bool UsePCA { get; set; }
        public bool UseResample { get; set; }
        public string ExportDirName { get; set; }

        public string GetExportRootPath() {
            if (ExportDirName == null)
                return Path.Combine(this.WorkingDir,
                    this.IsNormalMethod ? "Normal" : "Correlation",
                    this.IsLabeled ? "Labeled" : "Recognised",
                    this.Classifier.ToString(), this.PhysicalChar.ToString(), this.Activity.ToString(), Utility.UsedSensorString(this.UsedSensors));
            else
                return Path.Combine(this.WorkingDir, ExportDirName, 
                    this.Classifier.ToString(), this.PhysicalChar.ToString(), this.Activity.ToString(), Utility.UsedSensorString(this.UsedSensors));
        }
    }
}

﻿using System;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using ActivityAnalysisLibrary.Core;
using System.IO;
using ActivityAnalysisLibrary.IO;
using CubeDataSample.Core;
using System.Collections.Generic;
using System.Linq;
using ActivityAnalysisLibrary.ML;
using weka.core;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Collections.Concurrent;

namespace EstimateUsersAttributes {
    /// <summary>
    /// MainWindow.xaml の相互作用ロジック
    /// </summary>
    public partial class MainWindow : Window {
        public static ConsoleWindow CW;
        public const int USER_NUM = 61;
        public const string RESULT_DIR = "Result";
        public const string RANKING = "ranking";
        public const string AVE_LOGDENSITY = "aveLogDensity";
        public const string INFERRED_RESULT = "inferredResult";
        public const int TopNumForArff = 9; // arffにするときにtop-nを何個まで取得するか
        public const int MAX_TOP_NUM = 25; // 最大で取得できるtop-n の n
        public const string MAX_TOP_CSV = "maxTopNum_{0}_top.csv";
        public const string ATTRIBUTE_RANKING_CSV = "attributeRanking.csv";
        public const string ARFF_DIR = "arff";
        public const string TOTAL_INSTANCES_ARFF = "totalInstances.arff";
        public static readonly List<string> FILTER_METHODS = new List<string> { "None", "PrincipalComponents", "Resample", "RandomProjection" };

        MainManager mman;
        DataManagerForCube dman;
        List<UserData> sortedUsers;
        public static Dictionary<string, UserData> UserIDDict = new Dictionary<string, UserData>();
        PhysicalChar? checkedPhysicalChar = null;

        public MainWindow() {
            InitializeComponent();
            this.Left = 0; this.Top = 50;
            CW = new ConsoleWindow();
            CW.Left = this.Width; CW.Top = this.Top;
            CW.Show();
            foreach (var activty in Enum.GetValues(typeof(Activity)) as Activity[]) activityComboBox.Items.Add(activty);
            // 解析可能なメソッドをコンボボックスに列挙
            nominalMethodsComboBox.ItemsSource = Utility.NominalMethodsDict.Keys;
            numericalMethodsComboBox.ItemsSource = Utility.NumericMethodsDict.Keys;
            // arff の top-nを決めるコンボボックスに値を入れる
            changeTopNumComboBox.ItemsSource = Enumerable.Range(5, MAX_TOP_NUM).Where(i => i % 2 == 1);
        }

        #region GUI method

        private void addTabItem(TabItem tabItem, int index = -1) {
            if (tabItem == null) return;
            index = (index >= 0) ? index : mainTabControl.Items.Count;
            mainTabControl.Items.Insert(index, tabItem);
            mainTabControl.SelectedIndex = index;
        }

        private void Window_LocationChanged(object sender, EventArgs e) {
            if (CW == null) return;
            CW.Top = this.Top;
            CW.Left = this.Left + this.Width;
        }

        private void Window_Activated(object sender, EventArgs e) {
            if (CW == null) return; 
            CW.Activate();
            this.Activate();
        }

        private void Window_Closed(object sender, EventArgs e) {
            if (CW == null) return;
            CW.Close();
        }

        #endregion

        #region 1. Setting Tab
        // 設定したパスを保存
        private void folderFileChooseButton_Click(object sender, RoutedEventArgs e) {
            var clickedButton = sender as System.Windows.Controls.Button;
            if (clickedButton == null) return;
            string selectedPath = null;
            switch (clickedButton.Name) {
                case "mainXmlPathButton": {
                        selectedPath = Utility.SelectFilePath();
                        if (selectedPath != null) {
                            mainXmlPathTextBox.Text = selectedPath;
                            Properties.Settings.Default.MainWindow_MainXmlPath = selectedPath;
                        }
                    } break;
                case "sensorDataPathButton": {
                        selectedPath = Utility.SelectFolderPath();
                        if (selectedPath != null) {
                            sensorDataPathTextBox.Text = selectedPath;
                            Properties.Settings.Default.MainWindow_SensorDataPath = selectedPath;
                        }
                    } break;
                case "processedDataPathButton": {
                        selectedPath = Utility.SelectFolderPath();
                        if (selectedPath != null) {
                            processedDataPathTextBox.Text = selectedPath;
                            Properties.Settings.Default.MainWindow_ProcessedDataPath = selectedPath;
                        }
                    } break;
            }
            Properties.Settings.Default.Save();
        }
        // ユーザデータの読み込み
        private void processStartButton_Click(object sender, RoutedEventArgs e) {
            var mainXmlPath = mainXmlPathTextBox.Text;
            var sensorDataPath = sensorDataPathTextBox.Text;
            var processedDataPath = processedDataPathTextBox.Text;
            // 固まらないように別スレッド
            Task.Factory.StartNew(() => {
                if (File.Exists(mainXmlPath) && Directory.Exists(sensorDataPath) && Directory.Exists(processedDataPath)) {
                    CW.Print("Deserializing main manager");
                    // メインマネージャの設定
                    mman = MainManager.DeSerialize(mainXmlPath);
                    sortedUsers = mman.userman.userList;
                    sortedUsers.Sort(new UserDataComparer());
                    sortedUsers.ForEach(u => UserIDDict.Add(u.ID, u));
                    // データマネージャの設定
                    dman = new DataManagerForCube(mman, sensorDataPath, processedDataPath);
                    CW.Print("Loading Labels");
                    AnnotationLoader.LoadLabels(dman.LabelXmlFilePath(), sortedUsers);
                    string validatedResult = mman.ValidateSessionsAndLabels();
                    if (validatedResult.Length > 0) CW.Print("問題が発見されました\n" + validatedResult, ErrorStatus.Error);
                    mman.userman.AddAttributeKeyForToString("name");
                    // UIの変更
                    Dispatcher.BeginInvoke(new Action(()=>{
                        userListBox.ItemsSource = sortedUsers;
                        mainTabControl.SelectedIndex = 1;
                    }));
                } else {
                    CW.Print("Something wrong with main.xml path or sensor data path or processed data path", ErrorStatus.Error);
                }
            });
        }

        private void showResultsButton_Click(object sender, RoutedEventArgs e) {
            var filteringWindow = new FilteringPathWindow.FilteringPathWindow(showResultsPathTextBox.Text);
            filteringWindow.Show();
        }

        private void showResultsPathTextBox_PreviewDragOver(object sender, System.Windows.DragEventArgs e) {
            if (e.Data.GetDataPresent(System.Windows.DataFormats.FileDrop, true)) {
                e.Effects = System.Windows.DragDropEffects.Copy;
            } else {
                e.Effects = System.Windows.DragDropEffects.None;
            }
            e.Handled = true;
        }
        private void showResultsPathTextBox_Drop(object sender, System.Windows.DragEventArgs e) {
            var dropFiles = e.Data.GetData(System.Windows.DataFormats.FileDrop) as string[];
            if (dropFiles == null) return;
            showResultsPathTextBox.Text = dropFiles[0];
        }
        #endregion

        #region 2. Data Tab
        private void userListBox_SelectionChanged(object sender, SelectionChangedEventArgs e) {
            var selectedUser = userListBox.SelectedItem as UserData;
            if (selectedUser == null) return;
            userInfoTextBox.Text = selectedUser.ShowUserData();
            sessionListBox.Items.Clear();
            selectedUser.sessionList.ForEach(session => sessionListBox.Items.Add(session));
        }

        private void sessionListBox_SelectionChanged(object sender, SelectionChangedEventArgs e) {
            var selectedSession = sessionListBox.SelectedItem as SessionData;
            if (selectedSession == null) return;
            splittedSessionListBox.Items.Clear();
        }

        // 行動、センサの組み合わせと身体的な特徴を比較する
        private void compareUsersAttributesButton_Click(object sender, RoutedEventArgs e) {
            CalcSimilarityOfUsers compareUsersAttribute = null;
            foreach (var item in mainTabControl.Items) {
                if (item as CalcSimilarityOfUsers != null) {
                    compareUsersAttribute = item as CalcSimilarityOfUsers;
                    break;
                }
            }
            // tabItemからCompareUsersAttributeを削除する
            if (compareUsersAttribute != null) mainTabControl.Items.Remove(compareUsersAttribute);
            var tabItem = new TabItem();
            tabItem.Header = "3. " + CalcSimilarityOfUsers.CALC_SIM_DIR;
            compareUsersAttribute = new CalcSimilarityOfUsers(mman, dman, getSelectedUser());
            tabItem.Content = compareUsersAttribute;
            addTabItem(tabItem, 2);
        }
        
        private void naiveMethodButton_Click(object sender, RoutedEventArgs e) {
            var tabItem = new TabItem();
            tabItem.Header = "5. NaiveMethod";
            tabItem.Content = new ProcessResult(dman.cdman.rootpath, getSelectedUser());
            addTabItem(tabItem);
        }

        // スマートフォンから得た加速度データを抽出して推定する手法を実装
        private void inferFromFeatureVectorMethodButton_Click(object sender, RoutedEventArgs e) {
            var tabItem = new TabItem();
            tabItem.Header = "6. Infer from Feature Vector";
            tabItem.Content = new InferFromFeatureVector(mman, dman, getSelectedUser());
            addTabItem(tabItem);
        }

        List<UserData> getSelectedUser() {
            var selectedUsersList = new List<UserData>();
            foreach (var user in userListBox.SelectedItems) selectedUsersList.Add(user as UserData);
            return selectedUsersList;
        }
        #endregion

        #region 4. Process Similarity Table

        #region 1 類似度データを読み取り、ユーザ間のランキングを作成
        private void createUserRankingButton_Click(object sender, RoutedEventArgs e) {
            var defaultPath = (Directory.Exists(Properties.Settings.Default.MainWindow_PreviousSelectedPath)) ? Properties.Settings.Default.MainWindow_PreviousSelectedPath : dman.cdman.rootpath;
            string selectedRootDir = Utility.SelectFolderPath(defaultPath, "Choose Root Folder");
            if (selectedRootDir == null) return;
            Properties.Settings.Default.MainWindow_PreviousSelectedPath = selectedRootDir;
            Properties.Settings.Default.Save();
            // 選択したディレクトリ以下の「自転車」フォルダをすべて検索し、その親ディレクトリのパスを取得する
            Task.Factory.StartNew(() => {
                foreach (var bicyclingDirPath in Directory.EnumerateDirectories(selectedRootDir, Activity.Bicycling.ToString(), SearchOption.AllDirectories)){
                    var parentPath = Path.GetDirectoryName(bicyclingDirPath);
                    createUserRanking(parentPath);
                }
            });
        }

        void createUserRanking(string activitiesPath) {
            // User Dirだと親ディレクトリは"00"が含まれる
            var simiralityMethodName = Path.GetFileName(activitiesPath).Contains("00") ? Directory.GetParent(activitiesPath).Name : Path.GetFileName(activitiesPath);
            if (simiralityMethodName == RESULT_DIR) return;
            var isLogDensity = (simiralityMethodName == CalcSimilarityOfUsers.LOGDENSITY);

            Parallel.ForEach(Directory.GetDirectories(activitiesPath), activityDirPath => {
                var activityStr = Path.GetFileName(activityDirPath);
                if (activityStr == RESULT_DIR || activityStr == Activity.AllActivities.ToString() || activityStr == ARFF_DIR) return;

                #region ユーザ間の類似度ranking を作る
                var rankingDirPath = Path.Combine(activitiesPath, RESULT_DIR, activityStr, RANKING);
                Directory.CreateDirectory(rankingDirPath);
                // 類似度csv の読み取り
                foreach (var csvFilePath in Directory.GetFiles(activityDirPath, "*.csv")) {
                    var resultDict = CalcSimilarityOfUsers.LoadCsvSimilarityTable(csvFilePath, sortedUsers);
                    var originalFileName = Path.GetFileName(csvFilePath);

                    if (isLogDensity) {
                        var aveLogDensityPath = Path.Combine(activitiesPath, RESULT_DIR, activityStr, AVE_LOGDENSITY);
                        Directory.CreateDirectory(aveLogDensityPath);
                        #region 類似度テーブルのお互いの平均をとって、保存する
                        foreach (var rowTpl in sortedUsers.Select((u, i) => new { User = u, Idx = i })) {
                            foreach (var colTpl in sortedUsers.Select((u, i) => new { User = u, Idx = i })) {
                                if (rowTpl.Idx < colTpl.Idx) {
                                    var aveValue = (resultDict[rowTpl.User][colTpl.Idx].Item2 + resultDict[colTpl.User][rowTpl.Idx].Item2) / 2;
                                    resultDict[rowTpl.User][colTpl.Idx] = new Tuple<UserData, double>(colTpl.User, aveValue);
                                    resultDict[colTpl.User][rowTpl.Idx] = new Tuple<UserData, double>(rowTpl.User, aveValue);
                                }
                            }
                        }
                        // 保存
                        using (var sw = new StreamWriter(Path.Combine(aveLogDensityPath, "ave_" + originalFileName))) {
                            foreach (var user in sortedUsers) sw.WriteLine(user + "," + string.Join(",", resultDict[user].Select(t => t.Item2)));
                        }
                        #endregion
                    }

                    #region ランキングの書き込み
                    using (var sw = new StreamWriter(Path.Combine(rankingDirPath, "ranking_" + originalFileName))) {
                        foreach (var kv in resultDict) {
                            // cauchySchwarzは値が小さいと似ている
                            var orderedRow = (isLogDensity) ? kv.Value.OrderByDescending(t => t.Item2) : kv.Value.OrderBy(t => t.Item2);
                            // この時点で、自分がランキングから抜けるようにした where文
                            sw.WriteLine(kv.Key.ID + "," + string.Join(",", orderedRow.Where(t => t.Item1.ID != kv.Key.ID).Select(t => t.Item1.ID)));
                        }
                    }
                    #endregion
                }
                #endregion

            });
            CW.Print("Finished Creating User Ranking " + activitiesPath);
        }

        #endregion

        #region 2 ユーザのランキングを読み取り、身体的な特徴をtop1 - topNまで用いて推定
        private void inferPhysicalCharButton_Click(object sender, RoutedEventArgs e) {
            var defaultPath = (Directory.Exists(Properties.Settings.Default.MainWindow_PreviousSelectedPath)) ? Properties.Settings.Default.MainWindow_PreviousSelectedPath : dman.cdman.rootpath;
            string selectedRootDir = Utility.SelectFolderPath(defaultPath, "Choose Root Folder");
            if (selectedRootDir == null) return;
            Properties.Settings.Default.MainWindow_PreviousSelectedPath = selectedRootDir;
            Properties.Settings.Default.Save();
            Task.Factory.StartNew(() => {
                foreach (var resultPath in Directory.EnumerateDirectories(selectedRootDir, RESULT_DIR, SearchOption.AllDirectories)) {
                    inferPhysicalCharacteristics(resultPath);
                    CW.Print("Finished Infering Physical Char of " + resultPath);
                }
            });
        }

        void inferPhysicalCharacteristics(string resultPath) {
            Parallel.ForEach(Directory.GetDirectories(resultPath), activityDirPath => {
                var activityStr = Path.GetFileName(activityDirPath);
                if (activityStr == RESULT_DIR) throw new Exception("Something wrong with File structure");
                if (activityStr == ARFF_DIR) return;
                var rankingDirPath = Path.Combine(resultPath, activityStr, RANKING);
                if (Directory.Exists(rankingDirPath) == false) throw new Exception("No Directory " + rankingDirPath);
                var inferredResultDirPath = Path.Combine(resultPath, activityStr, INFERRED_RESULT);
                Directory.CreateDirectory(inferredResultDirPath);

                foreach (var rankingCsvFilePath in Directory.GetFiles(rankingDirPath, "*.csv")) {
                    var rankingCsvFileName = Path.GetFileName(rankingCsvFilePath);
                    var match = Regex.Match(rankingCsvFileName, @"\d\d\d\d");
                    if (match == null) continue;
                    // センサの種類を取得
                    var sensorString = match.Groups[0].ToString();
                    //ranking csv の読み取り
                    var rankingDict = LoadRankingData(rankingCsvFilePath);

                    // 身体的特徴のディレクトリをinferredResult以下に作成する
                    foreach (var physicalChar in Enum.GetValues(typeof(PhysicalChar)) as PhysicalChar[]) {
                        var sensorPatternDirPath = Path.Combine(inferredResultDirPath, physicalChar.ToString(), sensorString);
                        // もうすでに出力が完了しているからパス
                        if (physicalChar != PhysicalChar.pingpongExperienceNumeric && Directory.Exists(sensorPatternDirPath)) continue;
                        Directory.CreateDirectory(sensorPatternDirPath);
                        #region 類似度が高いユーザの身体的特徴を列挙（top-n）
                        // 1,3,5,7,9と生成していく
                        var topNumArray = Enumerable.Range(1, MAX_TOP_NUM).Where(i => i % 2 == 1).ToArray();
                        // 属性を保存する
                        using (var sw = new StreamWriter(Path.Combine(sensorPatternDirPath, string.Format("{0}_{1}", physicalChar, ATTRIBUTE_RANKING_CSV))))
                            foreach (var user in sortedUsers)
                                sw.WriteLine(user.ID + "," + string.Join(",", rankingDict[user].Select(u => Utility.GetPhysicalCharDataString(u, physicalChar))));

                        var topDict = new Dictionary<UserData, List<string>>();
                        if (Utility.IsNumericalPhysicalChar(physicalChar)) {
                            #region 数値的な特徴
                            foreach (var user in sortedUsers) {
                                topDict.Add(user, new List<string>());
                                foreach (int i in topNumArray)
                                    topDict[user].Add(rankingDict[user].Take(i).Select(u => Utility.GetNumericalPhysicalCharData(u, physicalChar)).Average().ToString());
                            }
                            #endregion
                        } else {
                            #region 離散的な特徴
                            foreach (var user in sortedUsers) {
                                topDict.Add(user, new List<string>());
                                foreach (int i in topNumArray)
                                    topDict[user].Add(MajorityOf(rankingDict[user].Take(i).Select(u => Utility.GetNominalPhysicalCharData(u, physicalChar))));
                            }
                            #endregion
                        }
                        using (var sw = new StreamWriter(Path.Combine(sensorPatternDirPath, string.Format(MAX_TOP_CSV, MAX_TOP_NUM))))
                            foreach (var user in sortedUsers)
                                sw.WriteLine(user.ID + "," + string.Join(",", topDict[user]));
                        #endregion
                    }
                }
                CW.Print("Finished With {0}", ErrorStatus.OK, activityStr);
            });
        }

        static Dictionary<UserData, List<UserData>> LoadRankingData(string rankingCsvPath) {
            var rankingDict = new Dictionary<UserData, List<UserData>>();
            using (var sr = new StreamReader(rankingCsvPath)) {
                while (sr.Peek() > 0) {
                    var splitline = sr.ReadLine().Split(',');
                    var mainUser = UserIDDict[splitline[0]];
                    rankingDict.Add(mainUser, new List<UserData>());
                    foreach (var userID in splitline.Skip(1))
                        rankingDict[mainUser].Add(UserIDDict[userID]);
                }
            }
            return rankingDict;
        }

        // top Nで多数決をしたもののうち、ユーザごとに最多のものを返す
        public static string MajorityOf(IEnumerable<string> attributeList) {
            var majorityDict = new Dictionary<string, int>();
            foreach (var attr in attributeList) {
                majorityDict[attr] = (majorityDict.ContainsKey(attr)) ? majorityDict[attr] + 1 : 1;
            }
            return majorityDict.Aggregate((l_kv, r_kv) => (l_kv.Value > r_kv.Value) ? l_kv : r_kv).Key;
        }
        #endregion

        #region 3 top-nのnを決めて、arffを作成
        private void createTopNArffButton_Click(object sender, RoutedEventArgs e) {
            var defaultPath = (Directory.Exists(Properties.Settings.Default.MainWindow_PreviousSelectedPath)) ? Properties.Settings.Default.MainWindow_PreviousSelectedPath : dman.cdman.rootpath;
            string selectedRootDir = Utility.SelectFolderPath(defaultPath, "Choose Root Folder");
            if (selectedRootDir == null) return;
            Properties.Settings.Default.MainWindow_PreviousSelectedPath = selectedRootDir;
            Properties.Settings.Default.Save();
            var top_num = (int)changeTopNumComboBox.SelectedItem;
            Task.Factory.StartNew(() => {
                foreach (var resultPath in Directory.EnumerateDirectories(selectedRootDir, RESULT_DIR, SearchOption.AllDirectories)) {
                    createTopNArff(resultPath, top_num);
                    CW.Print("Finished create top-{0} arff of {1}", ErrorStatus.OK, top_num, resultPath);
                }
            });
        }

        // Result内のすべての行動、センサの組み合わせにおけるtop-nの結果をArffに変換
        void createTopNArff(string resultPath, int top_num) {
            var allData = new ConcurrentDictionary<PhysicalChar, ConcurrentDictionary<UserData, ConcurrentQueue<Tuple<string, string>>>>();
            Parallel.ForEach(Directory.GetDirectories(resultPath), activityDirPath => {
                var activityStr = Path.GetFileName(activityDirPath);
                if (activityStr == RESULT_DIR) throw new Exception("Something wrong with File structure");
                if (activityStr == ARFF_DIR) return;
                foreach (var topFilePath in Directory.EnumerateFiles(Path.Combine(activityDirPath, INFERRED_RESULT), string.Format(MAX_TOP_CSV, MAX_TOP_NUM), SearchOption.AllDirectories)) {
                    var sensorDir = Directory.GetParent(topFilePath);
                    var sensorStr = sensorDir.Name;
                    var physicalChar = Utility.GetPhysicalCharFromString(sensorDir.Parent.Name);
                    // もうすでに出力が完了しているからパス
                    if(physicalChar != PhysicalChar.pingpongExperienceNumeric && Directory.Exists(Path.Combine(resultPath, ARFF_DIR, physicalChar.ToString()))) continue;

                    var topDict = new Dictionary<UserData, IEnumerable<string>>();
                    using (var sr = new StreamReader(topFilePath)) {
                        while (sr.Peek() > 0) {
                            var splitline = sr.ReadLine().Split(',');
                            topDict.Add(UserIDDict[splitline[0]], splitline.Skip(1));
                        }
                    }
                    if (allData.ContainsKey(physicalChar) == false) allData.TryAdd(physicalChar, new ConcurrentDictionary<UserData, ConcurrentQueue<Tuple<string, string>>>());
                    foreach (var user in sortedUsers) {
                        if (allData[physicalChar].ContainsKey(user) == false) allData[physicalChar].TryAdd(user, new ConcurrentQueue<Tuple<string, string>>());
                        // topNumは奇数であるため、+1したものは必ず２で割れる
                        var list = topDict[user].Take((top_num + 1) / 2).Select((strVal, i) => new { Value = strVal, TopNum = 2 * i + 1 });
                        foreach (var tpl in list)
                            allData[physicalChar][user].Enqueue(new Tuple<string, string>(string.Format("{0}_{1}_top{2}", activityStr, sensorStr, tpl.TopNum), tpl.Value));
                    }
                }
            });
            #region Arffの作成
            // Use physical char which was put in the allData dict
            foreach (var physicalChar in allData.Keys) {
                var arffPhysicalCharPath = Path.Combine(resultPath, ARFF_DIR, physicalChar.ToString());
                Directory.CreateDirectory(arffPhysicalCharPath);
                var isNumericalChar = Utility.IsNumericalPhysicalChar(physicalChar);
                var classTypeStr = isNumericalChar ? "numeric" : new StringBuilder().Append("{").Append(string.Join(",", Utility.GetNominalStringClass(physicalChar))).Append("}").ToString();
                foreach (var user in sortedUsers) {
                    var answerVal = isNumericalChar ? Utility.GetNumericalPhysicalCharData(user, physicalChar).ToString() : Utility.GetNominalPhysicalCharData(user, physicalChar);
                    var orderedAttrList = allData[physicalChar][user].OrderBy(tpl => tpl.Item1);
                    using (var sw = new StreamWriter(Path.Combine(arffPhysicalCharPath, user.ID + ".arff"))) {
                        sw.WriteLine("@relation ComparedResult");
                        sw.WriteLine();
                        foreach (var tpl in orderedAttrList) sw.WriteLine(string.Format( "@attribute {0} {1}", tpl.Item1, classTypeStr));
                        sw.WriteLine("@attribute class " + classTypeStr);
                        sw.WriteLine();
                        sw.WriteLine("@data");
                        sw.WriteLine(string.Join(",", orderedAttrList.Select(tpl => tpl.Item2)) + "," + answerVal);
                    }
                }
            }
            #endregion
        }
        #endregion

        #region  4 様々な高度な分析手法で解析
        private void SophisticatedAnalysisButton_Click(object sender, RoutedEventArgs e) {
            var defaultPath = (Directory.Exists(Properties.Settings.Default.MainWindow_PreviousSelectedPath)) ? Properties.Settings.Default.MainWindow_PreviousSelectedPath : dman.cdman.rootpath;
            string selectedRootDir = Utility.SelectFolderPath(defaultPath, "Choose Root Folder");
            if (selectedRootDir == null) return;
            Properties.Settings.Default.MainWindow_PreviousSelectedPath = selectedRootDir;
            Properties.Settings.Default.Save();

            var setting = new SophisticatedAnalysisSetting() {
                WorkingDir = dman.WekaWorkingRoot(),
                IsNormalMethod = normalMethodRadioButton.IsChecked.Value,
                IsLabeled = labeledDataRadioButton.IsChecked.Value,
                Classifier = getSelectedClassifier(),
                PhysicalChar = getCheckedUserPhysicalChar(),
                Activity = (Activity)activityComboBox.SelectedItem,
                UsedSensors = getUsedSensors(),
                UsePCA = pcaCheckBox.IsChecked.Value,
                UseResample = resampleCheckBox.IsChecked.Value
            };

            var isAllActivityPatternChecked = allActivityPatternCheckBox.IsChecked.Value;
            var isAllNumericalChecked = allNumericalCheckBox.IsChecked.Value;
            var isAllNominalChecked = allNominalCheckBox.IsChecked.Value;
            var isAllSensorPtn = setting.UsedSensors.All(flg => !flg); 

            Task.Factory.StartNew(() => {
                if(isAllActivityPatternChecked) {
                    #region 全行動
                    if (isAllNumericalChecked) {
                        #region 数値的な特徴をすべて
                        if (isAllSensorPtn)
                            foreach (var activity in Enum.GetValues(typeof(Activity)) as Activity[]) {
                                if (activity == Activity.AllActivities || activity == Activity.Running || activity == Activity.Sitting || activity == Activity.StairsUp || activity == Activity.StairsDown || activity == Activity.Standing || activity == Activity.Walking) continue;
                                foreach (var physChar in Utility.GetNumericalPhysicalCharList())
                                    foreach (var sensorIdx in Utility.AllSensorPatternIndices) {
                                        var newSetting = setting;
                                        newSetting.Activity = activity;
                                        newSetting.PhysicalChar = physChar;
                                        newSetting.UsedSensors = Utility.MakeFlags(sensorIdx);
                                        sophisticatedAnalysis(selectedRootDir, newSetting);
                                    }
                            }
                        else
                            foreach (var activity in Enum.GetValues(typeof(Activity)) as Activity[])
                                foreach (var physChar in Utility.GetNumericalPhysicalCharList()) {
                                    var newSetting = setting;
                                    newSetting.Activity = activity;
                                    newSetting.PhysicalChar = physChar;
                                    sophisticatedAnalysis(selectedRootDir, newSetting);
                                }
                        #endregion
                    } else if (isAllNominalChecked) {
                        #region ノミナルな特徴をすべて
                        if (isAllSensorPtn)
                            foreach (var activity in Enum.GetValues(typeof(Activity)) as Activity[])
                                foreach (var physChar in Utility.GetNominalPhysicalCharList())
                                    foreach (var sensorIdx in Utility.AllSensorPatternIndices) {
                                        var newSetting = setting;
                                        newSetting.Activity = activity;
                                        newSetting.PhysicalChar = physChar;
                                        newSetting.UsedSensors = Utility.MakeFlags(sensorIdx);
                                        sophisticatedAnalysis(selectedRootDir, newSetting);
                                    }
                        else
                            foreach (var activity in Enum.GetValues(typeof(Activity)) as Activity[])
                                foreach (var physChar in Utility.GetNominalPhysicalCharList()) {
                                    var newSetting = setting;
                                    newSetting.Activity = activity;
                                    newSetting.PhysicalChar = physChar;
                                    sophisticatedAnalysis(selectedRootDir, newSetting);
                                }
                        #endregion
                    } else {
                        #region 特徴どれか一つ
                        if (isAllSensorPtn)
                            foreach (var activity in Enum.GetValues(typeof(Activity)) as Activity[])
                                foreach (var sensorIdx in Utility.AllSensorPatternIndices) {
                                    var newSetting = setting;
                                    newSetting.Activity = activity;
                                    newSetting.UsedSensors = Utility.MakeFlags(sensorIdx);
                                    sophisticatedAnalysis(selectedRootDir, newSetting);
                                } 
                        else
                            foreach (var activity in Enum.GetValues(typeof(Activity)) as Activity[]) {
                                var newSetting = setting;
                                newSetting.Activity = activity;
                                sophisticatedAnalysis(selectedRootDir, newSetting);
                             }
                        #endregion
                    }
                    #endregion
                } else {
                    #region 行動１種類

                    if (isAllNumericalChecked) {
                        #region 数値的な特徴をすべて
                        if (isAllSensorPtn)
                            foreach (var physChar in Utility.GetNumericalPhysicalCharList())
                                foreach (var sensorIdx in Utility.AllSensorPatternIndices) {
                                    var newSetting = setting;
                                    newSetting.PhysicalChar = physChar;
                                    newSetting.UsedSensors = Utility.MakeFlags(sensorIdx);
                                    sophisticatedAnalysis(selectedRootDir, newSetting);
                                }
                        else
                            foreach (var physChar in Utility.GetNumericalPhysicalCharList()) {
                                var newSetting = setting;
                                newSetting.PhysicalChar = physChar;
                                sophisticatedAnalysis(selectedRootDir, newSetting);
                            }
                        #endregion
                    } else if (isAllNominalChecked) {
                        #region ノミナルな特徴をすべて
                        if (isAllSensorPtn)
                            foreach (var physChar in Utility.GetNominalPhysicalCharList())
                                foreach (var sensorIdx in Utility.AllSensorPatternIndices) {
                                    var newSetting = setting;
                                    newSetting.PhysicalChar = physChar;
                                    newSetting.UsedSensors = Utility.MakeFlags(sensorIdx);
                                    sophisticatedAnalysis(selectedRootDir, newSetting);
                                } else
                            foreach (var physChar in Utility.GetNominalPhysicalCharList()) {
                                var newSetting = setting;
                                newSetting.PhysicalChar = physChar;
                                sophisticatedAnalysis(selectedRootDir, newSetting);
                            }
                        #endregion
                    } else {
                        #region 特徴どれか一つ
                        if (isAllSensorPtn)
                            foreach (var sensorIdx in Utility.AllSensorPatternIndices) {
                                var newSetting = setting;
                                newSetting.UsedSensors = Utility.MakeFlags(sensorIdx);
                                sophisticatedAnalysis(selectedRootDir, newSetting);
                            }
                        else
                            sophisticatedAnalysis(selectedRootDir, setting);
                        #endregion
                    }

                    #endregion
                }
            });
        }

        // UIスレッド以外から呼び出し可能
        void sophisticatedAnalysis(string selectedRootPath, SophisticatedAnalysisSetting setting) {
            var method = new SophisticatedAnalysisMethod(mman, dman, sortedUsers, setting);
            var subDirs = Directory.GetDirectories(selectedRootPath);
            var isSubdirsUserdirs = false;
            // userIDがあったら、ユーザごとにテストデータとトレーニングデータが異なる
            if (Path.GetFileName(subDirs[0]).Contains("00")) isSubdirsUserdirs = true;

            if (isSubdirsUserdirs == false) {
                #region すべてのユーザにおいて、テストデータとトレーニングデータのソースが同じ
                var arffDirPath = Path.Combine(selectedRootPath, RESULT_DIR, ARFF_DIR, setting.PhysicalChar.ToString());
                method.AllInstancesDict = getActivitySensorFilteredInstancesDict(arffDirPath, setting);
                #endregion
            } else {
                #region ユーザごとにテストデータとトレーニングデータのソースが異なる
                var testAndTrainingInstances = new Dictionary<UserData, Tuple<Instances, List<Instances>>>();
                foreach (var userDirPath in subDirs) {
                    var userIDStr = Path.GetFileName(userDirPath);
                    var testUser = UserIDDict[userIDStr];
                    var arffDirPath = Path.Combine(userDirPath, RESULT_DIR, ARFF_DIR, setting.PhysicalChar.ToString());
                    var instancesDict = getActivitySensorFilteredInstancesDict(arffDirPath, setting);
                    var testInstances = instancesDict[testUser];
                    instancesDict.Remove(testUser);
                    testAndTrainingInstances[testUser] = new Tuple<Instances, List<Instances>>(testInstances, instancesDict.Values.ToList());
                }
                method.TestAndTrainingInstancesDict = testAndTrainingInstances;
                #endregion
            }
            method.ExecWekaLearnMethod(true, true, true, 5);
            CW.Print(string.Format("Finished executing method for {0} , {1} , {2}", 
                setting.PhysicalChar, setting.Activity, Utility.UsedSensorString(setting.UsedSensors)));
        }

        Dictionary<UserData, Instances> getActivitySensorFilteredInstancesDict(string arffDirPath, SophisticatedAnalysisSetting setting) {
            // Arffファイルを読み込むか
            var instancesDict = readArffFiles(arffDirPath);
            // 行動で制限
            if (setting.Activity != Activity.AllActivities) {
                var activityRegExpStr = "^" + setting.Activity + ".*";
                sortedUsers.ForEach(u => instancesDict[u] = Utility.ExtractAttributesByRegexp(instancesDict[u], activityRegExpStr));
            }
            // センサの組み合わせで制限( | でまたはの表現)
            var sensorRegExpStr = ".+_(" + string.Join("|", Utility.GetContainedSensorTypes(setting.UsedSensors)) + ")_+.";
            sortedUsers.ForEach(u => instancesDict[u] = Utility.ExtractAttributesByRegexp(instancesDict[u], sensorRegExpStr));
            return instancesDict;
        }

        Dictionary<UserData, Instances> createUserInstancesDict(string resultRootPath, PhysicalChar physicalChar, List<UserData> selectedUsers = null) {
            if (selectedUsers == null) selectedUsers = sortedUsers;
            var physCharClassVector = Utility.CreatePhysicalCharClassVector(physicalChar);

            #region インスタンスの定義を作成

            var attrList = new List<weka.core.Attribute>();
            foreach (var activityDirPath in Directory.GetDirectories(resultRootPath)) {
                var activityStr = Path.GetFileName(activityDirPath);
                if (activityStr == RESULT_DIR) continue;
                var physicalCharDirPath = Path.Combine(activityDirPath, INFERRED_RESULT, physicalChar.ToString());
                foreach (var sensorDirPath in Directory.EnumerateDirectories(physicalCharDirPath)) {
                    var sensorType = Path.GetFileName(sensorDirPath);
                    foreach (var csvFilePath in Directory.EnumerateFiles(sensorDirPath, "top*.csv").ToList()) {
                        var topType = Path.GetFileNameWithoutExtension(csvFilePath);
                        // 新しいattributeを定義してやる
                        var attrName = string.Join("_", new string[] { activityStr, sensorType, topType });
                        // 数値的な素性に対しては、physicalCharVectorが不必要
                        var newAttribute = (physCharClassVector == null) ? new weka.core.Attribute(attrName) : new weka.core.Attribute(attrName, physCharClassVector);
                        attrList.Add(newAttribute);
                    }
                }
            }
            var totalAttrNum = attrList.Count + 1;
            var attributesVector = new FastVector(totalAttrNum);
            foreach (var attribute in attrList) attributesVector.addElement(attribute);
            attributesVector.addElement((physCharClassVector == null) ? new weka.core.Attribute("class") : new weka.core.Attribute("class", physCharClassVector));

            #endregion

            var instancesDict = new Dictionary<UserData, Instances>();

            #region クラス（答え）の設定
            foreach (var user in selectedUsers) {
                var instances = new Instances("Relation", attributesVector, 1);
                instances.setClassIndex(totalAttrNum - 1);
                var instance = new Instance(totalAttrNum);
                if (Utility.IsNumericalPhysicalChar(physicalChar))
                    instance.setValue(instances.attribute(totalAttrNum - 1), Utility.GetNumericalPhysicalCharData(user, physicalChar));
                else
                    instance.setValue(instances.attribute(totalAttrNum - 1), Utility.GetNominalPhysicalCharData(user, physicalChar));
                instances.add(instance);
                instancesDict.Add(user, instances);
            }
            #endregion

            #region データの代入

            int attrIndex = 0;
            foreach (var activityDirPath in Directory.GetDirectories(resultRootPath)) {
                var activityStr = Path.GetFileName(activityDirPath);
                if (activityStr == RESULT_DIR) continue;
                var physicalCharDirPath = Path.Combine(activityDirPath, INFERRED_RESULT, physicalChar.ToString());
                foreach (var sensorDirPath in Directory.EnumerateDirectories(physicalCharDirPath)) {
                    var sensorType = Path.GetFileName(sensorDirPath);
                    foreach (var csvFilePath in Directory.EnumerateFiles(sensorDirPath, "top*.csv").ToList()) {
                        // top n csv の読み取り(一行だけでよい)
                        string[] eachUserInferredData = null;
                        using (var sr = new StreamReader(csvFilePath)) eachUserInferredData = sr.ReadLine().Split(',');
                        if (eachUserInferredData == null) continue;
                        //　ユーザ毎の推定結果をそれぞれ別のInstances保存する
                        foreach (var t in eachUserInferredData.Select((v, i) => new { Value = v, Index = i })) {
                            var user = selectedUsers[t.Index];
                            if (Utility.IsNumericalPhysicalChar(physicalChar)) {
                                double value;
                                if (Double.TryParse(t.Value, out value)) instancesDict[user].instance(0).setValue(instancesDict[user].attribute(attrIndex), value);
                            } else {
                                instancesDict[user].instance(0).setValue(instancesDict[user].attribute(attrIndex), t.Value);
                            }
                        }
                        attrIndex++;
                    }
                }
            }
            #endregion

            Instances totalInstaces = null;
            var arffExportDirPath = Path.Combine(resultRootPath, RESULT_DIR, "arff", physicalChar.ToString());
            foreach (var keyvalue in instancesDict) {
                var instances = keyvalue.Value;
                var path = Path.Combine(arffExportDirPath, keyvalue.Key.ID + ".arff");
                WekaUsefulMethods.SaveArff(instances, path);
                if (totalInstaces == null)
                    totalInstaces = new Instances(instances);
                else
                    for (var instanceIdx = 0; instanceIdx < instances.numInstances(); instanceIdx++)
                        totalInstaces.add(instances.instance(instanceIdx));
            }
            WekaUsefulMethods.SaveArff(totalInstaces, Path.Combine(arffExportDirPath, TOTAL_INSTANCES_ARFF));

            return instancesDict;
        }

        Dictionary<UserData, Instances> readArffFiles(string arffDirPath, List<UserData> selectedUser = null) {
            if (selectedUser == null) selectedUser = sortedUsers;
            if (Directory.Exists(arffDirPath) == false) throw new Exception("No such Dir");
            var instancesDict = new Dictionary<UserData, Instances>();
            // ユーザーの全Arffファイルのパスを取得する
            var filePathList = (Directory.GetFiles(arffDirPath, "*.arff", SearchOption.AllDirectories)).ToList();
            foreach (var user in selectedUser) {
                int removingIndex = -1;
                foreach (var arffFilePath in filePathList) {
                    removingIndex++;
                    var arffFileName = Path.GetFileName(arffFilePath);
                    // total arff  は user id が含まれないため、読み込まれることがない
                    if (arffFileName.Contains(user.ID)) {
                        var instances = WekaUsefulMethods.LoadArffFile(arffFilePath);
                        instances.setClassIndex(instances.numAttributes() - 1);
                        instancesDict.Add(user, instances);
                        break;
                    }
                }
                //  filePathListを毎回すべて確認するのは無駄だから、探し終えたものは削除
                filePathList.RemoveAt(removingIndex);
            }
            return instancesDict;
        }

        #endregion

        PhysicalChar getCheckedUserPhysicalChar(System.Windows.Controls.RadioButton checkedRadioButton = null) {
            // チェックボックスが指定されていない
            if (checkedRadioButton == null) {
                if (checkedPhysicalChar != null) return checkedPhysicalChar.Value;
                checkedRadioButton = attributeRadioButtonsGrid.Children.OfType<System.Windows.Controls.RadioButton>().FirstOrDefault(rdbtn => rdbtn.IsChecked.Value);
            }
            if (checkedRadioButton == ageRadioButton) return PhysicalChar.age;
            if (checkedRadioButton == heightRadioButton) return PhysicalChar.height;
            if (checkedRadioButton == weightRadioButton) return PhysicalChar.weight;
            if (checkedRadioButton == pingpongLevelRadioButton) return PhysicalChar.pingpongLevel;
            if (checkedRadioButton == pingpongExperienceNumericRadioButton) return PhysicalChar.pingpongExperienceNumeric;

            if (checkedRadioButton == genderRadioButton) return PhysicalChar.gender;
            if (checkedRadioButton == pingpongDominantHandRadioButton) return PhysicalChar.pingpongDominant;
            if (checkedRadioButton == writingDominantHandRadioButton) return PhysicalChar.writingDominant;
            if (checkedRadioButton == brushingDominantHandRadioButton) return PhysicalChar.brushingDominant;
            if (checkedRadioButton == vacuumingDominantHandRadioButton) return PhysicalChar.vacuumingDominant;
            if (checkedRadioButton == bikeTypeRadioButton) return PhysicalChar.bikeType;
            if (checkedRadioButton == chairTypeRadioButton) return PhysicalChar.chairType;
            if (checkedRadioButton == pingpoingExperienceRadioButton) return PhysicalChar.pingpoingExperience;
            if (checkedRadioButton == touchTypingRadioButton) return PhysicalChar.touchTyping;
            if (checkedRadioButton == washDishFreqRadioButton) return PhysicalChar.washDishFreq;
            if (checkedRadioButton == bikeFrequencyRadioButton) return PhysicalChar.bikeFrequency;
            if (checkedRadioButton == vacuumTypeRadioButton) return PhysicalChar.vacuumType;
            if (checkedRadioButton == devidedHeightRadioButton) return PhysicalChar.devidedHeight;
            throw new Exception("Something is wrong with radiobutton");
        }

        private void physicalCharRadioButton_Checked(object sender, RoutedEventArgs e) {
            // 最初の読み込み時にコンボボックスはnullになっている
            if (nominalMethodsComboBox != null && numericalMethodsComboBox != null) {
                checkedPhysicalChar = getCheckedUserPhysicalChar(sender as System.Windows.Controls.RadioButton);
                if (Utility.IsNumericalPhysicalChar(checkedPhysicalChar.Value)) {
                    nominalMethodsComboBox.IsEnabled = false;
                    numericalMethodsComboBox.IsEnabled = true;
                    pcaCheckBox.IsChecked = true;
                    pcaCheckBox.IsEnabled = true;
                    resampleCheckBox.IsEnabled = true;
                } else {
                    nominalMethodsComboBox.IsEnabled = true;
                    numericalMethodsComboBox.IsEnabled = false;
                    pcaCheckBox.IsChecked = false;
                    pcaCheckBox.IsEnabled = false;
                    resampleCheckBox.IsEnabled = false;
                }
            }
        }

        private void allNumericalCheckBox_Checked(object sender, RoutedEventArgs e) {
            nominalMethodsComboBox.IsEnabled = false;
            numericalMethodsComboBox.IsEnabled = true;
            pcaCheckBox.IsChecked = true;
            pcaCheckBox.IsEnabled = true;
            resampleCheckBox.IsEnabled = true;
        }

        private void allNominalCheckBox_Checked(object sender, RoutedEventArgs e) {
            nominalMethodsComboBox.IsEnabled = true;
            numericalMethodsComboBox.IsEnabled = false;
            pcaCheckBox.IsChecked = false;
            pcaCheckBox.IsEnabled = false;
            resampleCheckBox.IsEnabled = false;
        }

        WekaConstantParams.WekaClassifiers getSelectedClassifier() {
            if (nominalMethodsComboBox.IsEnabled)
                return Utility.GetClassifier(nominalMethodsComboBox.SelectedItem as string, true);
            else
                return Utility.GetClassifier(numericalMethodsComboBox.SelectedItem as string, false);
        }
        
        #region sensor checkbox events

        private bool[] getUsedSensors() {
            if (allPatternSensorCheckBox.IsChecked.Value) {
                return new bool[] { false, false, false, false };
            } else {
                return new bool[] {leftArmCheckBox.IsChecked.Value, rightArmCheckBox.IsChecked.Value,
                                            waistCheckBox.IsChecked.Value, thighCheckBox.IsChecked.Value};
            }
        }

        private void l_r_w_t_checkBox_Checked(object sender, RoutedEventArgs e) {
            bothArmsCheckBox.IsChecked = false;
            waistThighCheckBox.IsChecked = false;
            allSensorCheckBox.IsChecked = false;
            allPatternSensorCheckBox.IsChecked = false;
        }

        private void bothArmsCheckBox_Checked(object sender, System.Windows.RoutedEventArgs e) {
            leftArmCheckBox.IsChecked = true;
            rightArmCheckBox.IsChecked = true;
            waistCheckBox.IsChecked = false;
            thighCheckBox.IsChecked = false;
            waistThighCheckBox.IsChecked = false;
            allSensorCheckBox.IsChecked = false;
            allPatternSensorCheckBox.IsChecked = false;
        }

        private void waistThighCheckBox_Checked(object sender, System.Windows.RoutedEventArgs e) {
            leftArmCheckBox.IsChecked = false;
            rightArmCheckBox.IsChecked = false;
            waistCheckBox.IsChecked = true;
            thighCheckBox.IsChecked = true;
            bothArmsCheckBox.IsChecked = false;
            allSensorCheckBox.IsChecked = false;
            allPatternSensorCheckBox.IsChecked = false;
        }

        private void allSensorCheckBox_Checked(object sender, System.Windows.RoutedEventArgs e) {
            leftArmCheckBox.IsChecked = true;
            rightArmCheckBox.IsChecked = true;
            waistCheckBox.IsChecked = true;
            thighCheckBox.IsChecked = true;
            bothArmsCheckBox.IsChecked = false;
            waistThighCheckBox.IsChecked = false;
            allPatternSensorCheckBox.IsChecked = false;
        }

        private void allPatternSensorCheckBox_Checked(object sender, RoutedEventArgs e) {
            leftArmCheckBox.IsChecked = false;
            rightArmCheckBox.IsChecked = false;
            waistCheckBox.IsChecked = false;
            thighCheckBox.IsChecked = false;
            bothArmsCheckBox.IsChecked = false;
            waistThighCheckBox.IsChecked = false;
            allSensorCheckBox.IsChecked = false;
        }

        #endregion

        private void allActivityPatternCheckBox_Checked(object sender, RoutedEventArgs e) {
            activityComboBox.IsEnabled = false;
        }
        private void allActivityPatternCheckBox_Unchecked(object sender, RoutedEventArgs e) {
            activityComboBox.IsEnabled = true;
        }
        #endregion

    }

    // UserのIDの大小を比較する
    public class UserDataComparer : IComparer<UserData> {
        public int Compare(UserData x, UserData y) {
            int user1ID, user2ID;
            if (int.TryParse(x.ID, out user1ID) && int.TryParse(y.ID, out user2ID)) {
                if (user1ID > user2ID) return 1;
                else if (user1ID < user2ID) return -1;
                else return 0;
            } else {
                return 0;
            }
        }
    }
}

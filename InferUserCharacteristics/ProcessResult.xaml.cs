﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using ActivityAnalysisLibrary.Core;
using System.IO;

namespace EstimateUsersAttributes {
    /// <summary>
    /// NaiveMethodTab.xaml の相互作用ロジック
    /// </summary>
    public partial class ProcessResult : UserControl {
        public static readonly string NAIVE_METHOD = "NaiveMethod";

        string processFolderPath = null;
        List<UserData> selectedUsers = null;

        public ProcessResult(string processFolderPath, List<UserData> selectedUsers) {
            InitializeComponent();
            this.processFolderPath = processFolderPath;
            this.selectedUsers = selectedUsers;
            // IDの順に並べる
            this.selectedUsers.Sort(new UserDataComparer());
            selectedUsers.ForEach((user) => selectedUserListBox.Items.Add(user));
            foreach (var physicalChar in Enum.GetValues(typeof(PhysicalChar)) as PhysicalChar[]) {
                physicalCharComboBox.Items.Add(physicalChar);
            }
        }


        #region Naive Method
        string getNominalTraitMajority(List<UserData> trainingUsers, PhysicalChar physicalChar) {
            var rankingDict = new Dictionary<string, int>();
            trainingUsers.ForEach((user) => {
                string trait = Utility.GetNominalPhysicalCharData(user, physicalChar);
                if (rankingDict.ContainsKey(trait)) rankingDict[trait] += 1;
                else rankingDict.Add(trait, 1);
            });
            return rankingDict.Aggregate((leftkv, rightkv) => (leftkv.Value > rightkv.Value) ? leftkv : rightkv).Key;
        }

        double getNumericalTraitAverage(List<UserData> trainingUsers, PhysicalChar physicalChar) {
            return trainingUsers.Average((user) => Utility.GetNumericalPhysicalCharData(user, physicalChar));
        }

        private void naiveMethodButton_Click(object sender, RoutedEventArgs e) {
            Dictionary<PhysicalChar, List<Tuple<string, double, double, double>>> numericalEstimatedValDict = null;
            Dictionary<PhysicalChar, List<Tuple<string, string, string, bool>>> nominalEstimatedValDict = null;
            if (allPhysicalCharCheckBox.IsChecked.Value) {
                // All Char
                numericalEstimatedValDict = new Dictionary<PhysicalChar, List<Tuple<string, double, double, double>>>();
                nominalEstimatedValDict = new Dictionary<PhysicalChar, List<Tuple<string, string, string, bool>>>();
                foreach (var physChar in Enum.GetValues(typeof(PhysicalChar)) as PhysicalChar[]) {
                    if (Utility.IsNumericalPhysicalChar(physChar)) 
                        numericalEstimatedValDict.Add(physChar, new List<Tuple<string, double, double, double>>());
                    else
                        nominalEstimatedValDict.Add(physChar, new List<Tuple<string, string, string, bool>>());
                    foreach (var testUser in selectedUsers) {
                        var trainingUsers = selectedUsers.Where(user => user != testUser).ToList();
                        if (Utility.IsNumericalPhysicalChar(physChar)) {
                            var numericVal = Utility.GetNumericalPhysicalCharData(testUser, physChar);
                            var answerVal = getNumericalTraitAverage(trainingUsers, physChar);
                            numericalEstimatedValDict[physChar].Add(new Tuple<string, double, double, double>(testUser.ID, numericVal, answerVal, Math.Abs(numericVal - answerVal)));
                        } else {
                            var nominalVal = Utility.GetNominalPhysicalCharData(testUser, physChar);
                            var answerVal = getNominalTraitMajority(trainingUsers, physChar);
                            nominalEstimatedValDict[physChar].Add(new Tuple<string, string, string, bool>(testUser.ID, nominalVal, answerVal, (nominalVal == answerVal) ? true : false));
                        }
                    }
                }

            } else {
                var selectedPhysicalChar = (PhysicalChar)physicalCharComboBox.SelectedItem;
                if (Utility.IsNumericalPhysicalChar(selectedPhysicalChar)) {
                    numericalEstimatedValDict = new Dictionary<PhysicalChar, List<Tuple<string, double, double, double>>>();
                    numericalEstimatedValDict.Add(selectedPhysicalChar, new List<Tuple<string, double, double, double>>());
                    foreach (var testUser in selectedUsers) {
                        var trainingUsers = selectedUsers.Where(user => user != testUser).ToList();
                        var numericVal = Utility.GetNumericalPhysicalCharData(testUser, selectedPhysicalChar);
                        var answerVal = getNumericalTraitAverage(trainingUsers, selectedPhysicalChar);
                        numericalEstimatedValDict[selectedPhysicalChar].Add(new Tuple<string, double, double, double>(testUser.ID, numericVal, answerVal, Math.Abs(numericVal - answerVal)));
                    }
                } else {
                    nominalEstimatedValDict = new Dictionary<PhysicalChar, List<Tuple<string, string, string, bool>>>();
                    nominalEstimatedValDict.Add(selectedPhysicalChar, new List<Tuple<string, string, string, bool>>());
                    foreach (var testUser in selectedUsers) {
                        var trainingUsers = selectedUsers.Where(user => user != testUser).ToList();
                        var nominalVal = Utility.GetNominalPhysicalCharData(testUser, selectedPhysicalChar);
                        var answerVal = getNominalTraitMajority(trainingUsers, selectedPhysicalChar);
                        nominalEstimatedValDict[selectedPhysicalChar].Add(new Tuple<string, string, string, bool>(testUser.ID, nominalVal, answerVal, (nominalVal == answerVal) ? true : false));
                    }
                }
            }
            if (numericalEstimatedValDict != null) exportNumericalEstimatedValDict(numericalEstimatedValDict);
            if (nominalEstimatedValDict != null) exportNominalEstimatedValDict(nominalEstimatedValDict);
            MainWindow.CW.Print("Finished Exporting : " + Path.Combine(processFolderPath, NAIVE_METHOD));
        }

        void exportNumericalEstimatedValDict(Dictionary<PhysicalChar, List<Tuple<string, double, double, double>>> numericalEstimatedValDict) {
            var exportRootPath = Path.Combine(processFolderPath, NAIVE_METHOD);
            Directory.CreateDirectory(exportRootPath);
            foreach (var kv in numericalEstimatedValDict) {
                using (var sw = new StreamWriter(Path.Combine(exportRootPath, kv.Key + ".csv"))) {
                    sw.WriteLine("User :" + string.Join(",", kv.Value.Select(t => t.Item1)));
                    sw.WriteLine("Value :" + string.Join(",", kv.Value.Select(t => t.Item2)));
                    sw.WriteLine("Estimated :" + string.Join(",", kv.Value.Select(t => t.Item3)));
                    var errorList = kv.Value.Select(t => t.Item4).ToList();
                    sw.WriteLine("Error :" + string.Join(",", errorList));
                    sw.WriteLine("Result :" + errorList.Average());
                }
            }
        }

        void exportNominalEstimatedValDict(Dictionary<PhysicalChar, List<Tuple<string, string, string, bool>>> nominalEstimatedValDict) {
            var exportRootPath = Path.Combine(processFolderPath, NAIVE_METHOD);
            Directory.CreateDirectory(exportRootPath);
            foreach (var kv in nominalEstimatedValDict) {
                using (var sw = new StreamWriter(Path.Combine(exportRootPath, kv.Key + ".csv"))) {
                    sw.WriteLine("User :" + string.Join(",", kv.Value.Select(t => t.Item1)));
                    sw.WriteLine("Value :" + string.Join(",", kv.Value.Select(t => t.Item2)));
                    sw.WriteLine("Estimated :" + string.Join(",", kv.Value.Select(t => t.Item3)));
                    var booleanList = kv.Value.Select(t => t.Item4).ToList();
                    sw.WriteLine("Error :" + string.Join(",", booleanList));
                    sw.WriteLine("Result :" + (booleanList.Where(ans => ans).Count() / (double)booleanList.Count) * 100 );
                }
            }
        }

        #endregion

        private void dragAndDropLabel_PreviewDragOver(object sender, DragEventArgs e)
        {
            if (e.Data.GetDataPresent(DataFormats.FileDrop, true))
            {
                e.Effects = DragDropEffects.Copy;
            }
            else
            {
                e.Effects = DragDropEffects.None;
            }
            e.Handled = true;
        }

        private void dragAndDropLabel_Drop(object sender, DragEventArgs e)
        {
            var dropFiles = e.Data.GetData(DataFormats.FileDrop) as string[];
            if (dropFiles == null) return;
            dragAndDropPathTextBlock.Text = dropFiles[0];
        }

        private void processErrataButton_Click(object sender, RoutedEventArgs e)
        {
            var errataPath = dragAndDropPathTextBlock.Text;
            if(File.Exists(errataPath) == false) return;
            List<string> answerList = null;
            List<bool> errata = null;
            using (var sr = new StreamReader(errataPath))
            {
                answerList = sr.ReadLine().Split(',').ToList();
                sr.ReadLine();
                errata = sr.ReadLine().Split(',').Select(str => str == "True").ToList();
            }
            var countDict = new Dictionary<string, Tuple<int,int>>();
            for (var userIdx = 0; userIdx < answerList.Count; userIdx++)
            {
                var ansStr = answerList[userIdx];
                var addedVal = errata[userIdx] ? 1 : 0;
                countDict[ansStr] = (countDict.ContainsKey(ansStr)) ? new Tuple<int, int>(countDict[ansStr].Item1 + addedVal, countDict[ansStr].Item2 + 1) : new Tuple<int, int>(addedVal, 1);
            }
            // 属性ごとの正答率を出力
            using (var sw = new StreamWriter(errataPath, true))
                sw.WriteLine(string.Join(", ", countDict.ToList().Select(kv => string.Format("{0} {1}/{2} {3}", kv.Key, kv.Value.Item1, kv.Value.Item2, ((double)kv.Value.Item1) / kv.Value.Item2))));
        }
    
    
    }
}

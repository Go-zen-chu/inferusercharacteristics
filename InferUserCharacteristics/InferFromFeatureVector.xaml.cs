﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows;
using System.Windows.Controls;
using ActivityAnalysisLibrary.Core;
using CubeDataSample.Core;
using weka.core;
using System.Threading.Tasks;
using System.Collections.Concurrent;
using ActivityAnalysisLibrary.ML;

namespace EstimateUsersAttributes {
    /// <summary>
    /// InferFromFeatureVector.xaml の相互作用ロジック
    /// </summary>
    public partial class InferFromFeatureVector : UserControl {
        public const string INFER_FROM_FEATURE_VECTOR = "InferFromFeatureVector";
        MainManager mman;
        DataManagerForCube dman;
        List<UserData> selectedUsers = null;
        Dictionary<UserData, Instances> originalInstancesDict = new Dictionary<UserData,Instances>();
        List<PhysicalChar> inferringPhysicalChars = new List<PhysicalChar>() { PhysicalChar.gender, PhysicalChar.height, PhysicalChar.weight };

        public InferFromFeatureVector(MainManager mainManager, DataManagerForCube dataManager, List<UserData> selectedUsers) {
            if (mainManager == null || dataManager == null || selectedUsers == null) return;
            InitializeComponent();
            this.mman = mainManager;
            this.dman = dataManager;
            this.selectedUsers = selectedUsers;
            foreach (var selectedUser in selectedUsers) selectedUserListBox.Items.Add(selectedUser);
            // IDの順に並べる
            this.selectedUsers.Sort(new UserDataComparer());
            nominalMethodsComboBox.ItemsSource = Utility.NominalMethodsDict.Keys;
            numericalMethodsComboBox.ItemsSource = Utility.NumericMethodsDict.Keys;
        }

        #region sensor checkbox events

        private bool[] getUsedSensors() {
            if (allPatternSensorCheckBox.IsChecked.Value) {
                return new bool[] { false, false, false, false };
            } else {
                return new bool[] {leftArmCheckBox.IsChecked.Value, rightArmCheckBox.IsChecked.Value,
                                            waistCheckBox.IsChecked.Value, thighCheckBox.IsChecked.Value};
            }
        }

        private void l_r_w_t_checkBox_Checked(object sender, RoutedEventArgs e) {
            bothArmsCheckBox.IsChecked = false;
            waistThighCheckBox.IsChecked = false;
            allSensorCheckBox.IsChecked = false;
            allPatternSensorCheckBox.IsChecked = false;
        }

        private void bothArmsCheckBox_Checked(object sender, System.Windows.RoutedEventArgs e) {
            leftArmCheckBox.IsChecked = true;
            rightArmCheckBox.IsChecked = true;
            waistCheckBox.IsChecked = false;
            thighCheckBox.IsChecked = false;
            waistThighCheckBox.IsChecked = false;
            allSensorCheckBox.IsChecked = false;
            allPatternSensorCheckBox.IsChecked = false;
        }

        private void waistThighCheckBox_Checked(object sender, System.Windows.RoutedEventArgs e) {
            leftArmCheckBox.IsChecked = false;
            rightArmCheckBox.IsChecked = false;
            waistCheckBox.IsChecked = true;
            thighCheckBox.IsChecked = true;
            bothArmsCheckBox.IsChecked = false;
            allSensorCheckBox.IsChecked = false;
            allPatternSensorCheckBox.IsChecked = false;
        }

        private void allSensorCheckBox_Checked(object sender, System.Windows.RoutedEventArgs e) {
            leftArmCheckBox.IsChecked = true;
            rightArmCheckBox.IsChecked = true;
            waistCheckBox.IsChecked = true;
            thighCheckBox.IsChecked = true;
            bothArmsCheckBox.IsChecked = false;
            waistThighCheckBox.IsChecked = false;
            allPatternSensorCheckBox.IsChecked = false;
        }

        private void allPatternSensorCheckBox_Checked(object sender, RoutedEventArgs e) {
            leftArmCheckBox.IsChecked = false;
            rightArmCheckBox.IsChecked = false;
            waistCheckBox.IsChecked = false;
            thighCheckBox.IsChecked = false;
            bothArmsCheckBox.IsChecked = false;
            waistThighCheckBox.IsChecked = false;
            allSensorCheckBox.IsChecked = false;
        }

        #endregion

        private void startProcessButton_Click(object sender, RoutedEventArgs e) {
            // 特徴ベクトルを学習し、直接推定を行う
            var basicSetting = new SophisticatedAnalysisSetting() {
                WorkingDir = dman.WekaWorkingRoot(),
                UsedSensors = getUsedSensors(),
                UsePCA = pcaCheckBox.IsChecked.Value,
                UseResample = resampleCheckBox.IsChecked.Value,
                ExportDirName = INFER_FROM_FEATURE_VECTOR
            };
            var isAllSensorPtn = basicSetting.UsedSensors.All(flg => !flg);

            Task.Factory.StartNew(() => {
                // 全てのarffの読み込み
                Utility.LoadOriginalArffFiles(dman.ArffDataRoot(), selectedUsers, ref originalInstancesDict);

                // 行動とセンサでデータを制限する
                foreach (var activity in new List<Activity>() { Activity.AllActivities }) {
                    var replacedInstancesDict = new ConcurrentDictionary<UserData, Instances>();
                    // Instances のクラスをユーザの身体的特徴に置換する
                    var activityExtractedInstancesDict = Utility.ExtractByActivityAndSensors(originalInstancesDict, selectedUsers, activity, basicSetting.UsedSensors);
                    foreach (var physChar in inferringPhysicalChars) {
                        var setting = basicSetting;
                        setting.Activity = activity; setting.PhysicalChar = physChar;
                        foreach (var user in selectedUsers) {
                            var replacedInstances = new Instances(activityExtractedInstancesDict[user]);
                            if (Utility.IsNumericalPhysicalChar(physChar)) {
                                var usersPhysChar = Utility.GetNumericalPhysicalCharData(user, physChar);
                                // class が削除されているので、insertで代入
                                replacedInstances.insertAttributeAt(new weka.core.Attribute("class"), replacedInstances.numAttributes());
                                replacedInstances.setClassIndex(replacedInstances.numAttributes() - 1);
                                for (int instanceIdx = 0; instanceIdx < replacedInstances.numInstances(); instanceIdx++) replacedInstances.instance(instanceIdx).setClassValue(usersPhysChar);
                            } else {
                                var usersPhysChar = Utility.GetNominalPhysicalCharData(user, physChar);
                                // class が削除されているので、insertで代入
                                replacedInstances.insertAttributeAt(new weka.core.Attribute("class", Utility.CreatePhysicalCharClassVector(physChar)), replacedInstances.numAttributes());
                                replacedInstances.setClassIndex(replacedInstances.numAttributes() - 1);
                                for (int instanceIdx = 0; instanceIdx < replacedInstances.numInstances(); instanceIdx++) replacedInstances.instance(instanceIdx).setClassValue(usersPhysChar);
                            }
                            replacedInstancesDict[user] = replacedInstances;
                        }
                        //var testingClassifiers = Utility.IsNumericalPhysicalChar(physChar) ? Utility.NumericMethodsDict.Values : Utility.NominalMethodsDict.Values;
                        //Parallel.ForEach(testingClassifiers, classifier => {
                        //    var differentClassifierSetting = setting;
                        //    differentClassifierSetting.Classifier = classifier;
                        //    var method = new SophisticatedAnalysisMethod(mman, dman, selectedUsers, differentClassifierSetting);
                        //    method.AllInstancesConcurrentDict = replacedInstancesDict;
                        //    var startMethodTime = DateTime.Now;
                        //    MainWindow.CW.Print("Start learning {0} : {1} ", ErrorStatus.OK, differentClassifierSetting.PhysicalChar, classifier);
                        //    method.ExecWekaLearnMethod(true, true, true, 10);
                        //    MainWindow.CW.Print("End learning {0} : {1} : Takes {2}", ErrorStatus.OK, differentClassifierSetting.PhysicalChar, classifier, DateTime.Now - startMethodTime);
                        //});
                        var classifier = WekaConstantParams.WekaClassifiers.IBk;
                        var differentClassifierSetting = setting;
                        differentClassifierSetting.Classifier = classifier;
                        var method = new SophisticatedAnalysisMethod(mman, dman, selectedUsers, differentClassifierSetting);
                        method.AllInstancesConcurrentDict = replacedInstancesDict;
                        var startMethodTime = DateTime.Now;
                        MainWindow.CW.Print("Start learning {0} : {1} ", ErrorStatus.OK, differentClassifierSetting.PhysicalChar, classifier);
                        method.ExecWekaLearnMethod(true, true, true, 10);
                        MainWindow.CW.Print("End learning {0} : {1} : Takes {2}", ErrorStatus.OK, differentClassifierSetting.PhysicalChar, classifier, DateTime.Now - startMethodTime);
                    }
                }
            });
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using ActivityAnalysisLibrary.Core;
using weka.core;
using System.IO;
using ActivityAnalysisLibrary.ML;
using System.Text.RegularExpressions;
using System.Windows;
using System.ComponentModel;
using System.Text;
using System.Xml.Serialization;
using CubeDataSample.Core;
using System.Threading.Tasks;
using System.Threading;
using System.Collections.Concurrent;
using System.Runtime.Serialization.Formatters.Binary;
using weka.clusterers;

namespace EstimateUsersAttributes {
    /// <summary>
    /// ユーザ間の特徴ベクトルの類似度を計算する
    /// </summary>
    public partial class CalcSimilarityOfUsers : System.Windows.Controls.UserControl {
        public readonly string[] ATTRIBUTE_LIST = new string[] { "Mean_1_X", "Mean_1_Y", "Mean_1_Z", "Var_1_X", "Var_1_Y", "Var_1_Z", "FFTX_1_0", "FFTX_1_1", "FFTX_1_2", "FFTX_1_3", "FFTX_1_4", "FFTX_1_5", "FFTX_1_6", "FFTX_1_7", "FFTX_1_8", "FFTX_1_9", "FFTX_1_10", "FFTX_1_11", "FFTX_1_12", "FFTX_1_13", "FFTX_1_14", "FFTX_1_15", "FFTX_1_16", "FFTX_1_17", "FFTX_1_18", "FFTX_1_19", "FFTX_1_20", "FFTX_1_21", "FFTX_1_22", "FFTX_1_23", "FFTX_1_24", "FFTX_1_25", "FFTX_1_26", "FFTX_1_27", "FFTX_1_28", "FFTX_1_29", "FFTX_1_30", "FFTX_1_31", "FFTY_1_0", "FFTY_1_1", "FFTY_1_2", "FFTY_1_3", "FFTY_1_4", "FFTY_1_5", "FFTY_1_6", "FFTY_1_7", "FFTY_1_8", "FFTY_1_9", "FFTY_1_10", "FFTY_1_11", "FFTY_1_12", "FFTY_1_13", "FFTY_1_14", "FFTY_1_15", "FFTY_1_16", "FFTY_1_17", "FFTY_1_18", "FFTY_1_19", "FFTY_1_20", "FFTY_1_21", "FFTY_1_22", "FFTY_1_23", "FFTY_1_24", "FFTY_1_25", "FFTY_1_26", "FFTY_1_27", "FFTY_1_28", "FFTY_1_29", "FFTY_1_30", "FFTY_1_31", "FFTZ_1_0", "FFTZ_1_1", "FFTZ_1_2", "FFTZ_1_3", "FFTZ_1_4", "FFTZ_1_5", "FFTZ_1_6", "FFTZ_1_7", "FFTZ_1_8", "FFTZ_1_9", "FFTZ_1_10", "FFTZ_1_11", "FFTZ_1_12", "FFTZ_1_13", "FFTZ_1_14", "FFTZ_1_15", "FFTZ_1_16", "FFTZ_1_17", "FFTZ_1_18", "FFTZ_1_19", "FFTZ_1_20", "FFTZ_1_21", "FFTZ_1_22", "FFTZ_1_23", "FFTZ_1_24", "FFTZ_1_25", "FFTZ_1_26", "FFTZ_1_27", "FFTZ_1_28", "FFTZ_1_29", "FFTZ_1_30", "FFTZ_1_31", "Mean_2_X", "Mean_2_Y", "Mean_2_Z", "Var_2_X", "Var_2_Y", "Var_2_Z", "FFTX_2_0", "FFTX_2_1", "FFTX_2_2", "FFTX_2_3", "FFTX_2_4", "FFTX_2_5", "FFTX_2_6", "FFTX_2_7", "FFTX_2_8", "FFTX_2_9", "FFTX_2_10", "FFTX_2_11", "FFTX_2_12", "FFTX_2_13", "FFTX_2_14", "FFTX_2_15", "FFTX_2_16", "FFTX_2_17", "FFTX_2_18", "FFTX_2_19", "FFTX_2_20", "FFTX_2_21", "FFTX_2_22", "FFTX_2_23", "FFTX_2_24", "FFTX_2_25", "FFTX_2_26", "FFTX_2_27", "FFTX_2_28", "FFTX_2_29", "FFTX_2_30", "FFTX_2_31", "FFTY_2_0", "FFTY_2_1", "FFTY_2_2", "FFTY_2_3", "FFTY_2_4", "FFTY_2_5", "FFTY_2_6", "FFTY_2_7", "FFTY_2_8", "FFTY_2_9", "FFTY_2_10", "FFTY_2_11", "FFTY_2_12", "FFTY_2_13", "FFTY_2_14", "FFTY_2_15", "FFTY_2_16", "FFTY_2_17", "FFTY_2_18", "FFTY_2_19", "FFTY_2_20", "FFTY_2_21", "FFTY_2_22", "FFTY_2_23", "FFTY_2_24", "FFTY_2_25", "FFTY_2_26", "FFTY_2_27", "FFTY_2_28", "FFTY_2_29", "FFTY_2_30", "FFTY_2_31", "FFTZ_2_0", "FFTZ_2_1", "FFTZ_2_2", "FFTZ_2_3", "FFTZ_2_4", "FFTZ_2_5", "FFTZ_2_6", "FFTZ_2_7", "FFTZ_2_8", "FFTZ_2_9", "FFTZ_2_10", "FFTZ_2_11", "FFTZ_2_12", "FFTZ_2_13", "FFTZ_2_14", "FFTZ_2_15", "FFTZ_2_16", "FFTZ_2_17", "FFTZ_2_18", "FFTZ_2_19", "FFTZ_2_20", "FFTZ_2_21", "FFTZ_2_22", "FFTZ_2_23", "FFTZ_2_24", "FFTZ_2_25", "FFTZ_2_26", "FFTZ_2_27", "FFTZ_2_28", "FFTZ_2_29", "FFTZ_2_30", "FFTZ_2_31", "Mean_3_X", "Mean_3_Y", "Mean_3_Z", "Var_3_X", "Var_3_Y", "Var_3_Z", "FFTX_3_0", "FFTX_3_1", "FFTX_3_2", "FFTX_3_3", "FFTX_3_4", "FFTX_3_5", "FFTX_3_6", "FFTX_3_7", "FFTX_3_8", "FFTX_3_9", "FFTX_3_10", "FFTX_3_11", "FFTX_3_12", "FFTX_3_13", "FFTX_3_14", "FFTX_3_15", "FFTX_3_16", "FFTX_3_17", "FFTX_3_18", "FFTX_3_19", "FFTX_3_20", "FFTX_3_21", "FFTX_3_22", "FFTX_3_23", "FFTX_3_24", "FFTX_3_25", "FFTX_3_26", "FFTX_3_27", "FFTX_3_28", "FFTX_3_29", "FFTX_3_30", "FFTX_3_31", "FFTY_3_0", "FFTY_3_1", "FFTY_3_2", "FFTY_3_3", "FFTY_3_4", "FFTY_3_5", "FFTY_3_6", "FFTY_3_7", "FFTY_3_8", "FFTY_3_9", "FFTY_3_10", "FFTY_3_11", "FFTY_3_12", "FFTY_3_13", "FFTY_3_14", "FFTY_3_15", "FFTY_3_16", "FFTY_3_17", "FFTY_3_18", "FFTY_3_19", "FFTY_3_20", "FFTY_3_21", "FFTY_3_22", "FFTY_3_23", "FFTY_3_24", "FFTY_3_25", "FFTY_3_26", "FFTY_3_27", "FFTY_3_28", "FFTY_3_29", "FFTY_3_30", "FFTY_3_31", "FFTZ_3_0", "FFTZ_3_1", "FFTZ_3_2", "FFTZ_3_3", "FFTZ_3_4", "FFTZ_3_5", "FFTZ_3_6", "FFTZ_3_7", "FFTZ_3_8", "FFTZ_3_9", "FFTZ_3_10", "FFTZ_3_11", "FFTZ_3_12", "FFTZ_3_13", "FFTZ_3_14", "FFTZ_3_15", "FFTZ_3_16", "FFTZ_3_17", "FFTZ_3_18", "FFTZ_3_19", "FFTZ_3_20", "FFTZ_3_21", "FFTZ_3_22", "FFTZ_3_23", "FFTZ_3_24", "FFTZ_3_25", "FFTZ_3_26", "FFTZ_3_27", "FFTZ_3_28", "FFTZ_3_29", "FFTZ_3_30", "FFTZ_3_31", "Mean_4_X", "Mean_4_Y", "Mean_4_Z", "Var_4_X", "Var_4_Y", "Var_4_Z", "FFTX_4_0", "FFTX_4_1", "FFTX_4_2", "FFTX_4_3", "FFTX_4_4", "FFTX_4_5", "FFTX_4_6", "FFTX_4_7", "FFTX_4_8", "FFTX_4_9", "FFTX_4_10", "FFTX_4_11", "FFTX_4_12", "FFTX_4_13", "FFTX_4_14", "FFTX_4_15", "FFTX_4_16", "FFTX_4_17", "FFTX_4_18", "FFTX_4_19", "FFTX_4_20", "FFTX_4_21", "FFTX_4_22", "FFTX_4_23", "FFTX_4_24", "FFTX_4_25", "FFTX_4_26", "FFTX_4_27", "FFTX_4_28", "FFTX_4_29", "FFTX_4_30", "FFTX_4_31", "FFTY_4_0", "FFTY_4_1", "FFTY_4_2", "FFTY_4_3", "FFTY_4_4", "FFTY_4_5", "FFTY_4_6", "FFTY_4_7", "FFTY_4_8", "FFTY_4_9", "FFTY_4_10", "FFTY_4_11", "FFTY_4_12", "FFTY_4_13", "FFTY_4_14", "FFTY_4_15", "FFTY_4_16", "FFTY_4_17", "FFTY_4_18", "FFTY_4_19", "FFTY_4_20", "FFTY_4_21", "FFTY_4_22", "FFTY_4_23", "FFTY_4_24", "FFTY_4_25", "FFTY_4_26", "FFTY_4_27", "FFTY_4_28", "FFTY_4_29", "FFTY_4_30", "FFTY_4_31", "FFTZ_4_0", "FFTZ_4_1", "FFTZ_4_2", "FFTZ_4_3", "FFTZ_4_4", "FFTZ_4_5", "FFTZ_4_6", "FFTZ_4_7", "FFTZ_4_8", "FFTZ_4_9", "FFTZ_4_10", "FFTZ_4_11", "FFTZ_4_12", "FFTZ_4_13", "FFTZ_4_14", "FFTZ_4_15", "FFTZ_4_16", "FFTZ_4_17", "FFTZ_4_18", "FFTZ_4_19", "FFTZ_4_20", "FFTZ_4_21", "FFTZ_4_22", "FFTZ_4_23", "FFTZ_4_24", "FFTZ_4_25", "FFTZ_4_26", "FFTZ_4_27", "FFTZ_4_28", "FFTZ_4_29", "FFTZ_4_30", "FFTZ_4_31" };

        // Correlationで用いる変数
        public static readonly string CORRELATION_DIRNAME = "Correlation";
        public static readonly string XML_EXT = ".xml";
        public static readonly string NEGATIVE_CORRELATION_ATTRIBUTES = "NegativeCorrelationAttributes";
        public static readonly string USING_ATTRIBUTES = "usingAttributes";
        public static readonly string USING_ATTRIBUTES_RANKING = "usingAttributesRanking";
        public static readonly int ATTRIBUTE_NUM = 408;
        public static readonly int top_num = 5;

        // log densityで用いるEMクラスタの数
        public static readonly int EM_CLUSTER_NUM = 5;
        // Caushy-Schwarzで用いるクラスタの数
        public static readonly int GMM_CLUSTERING_NUM = 5;
        public static readonly string CLUSTERING_OPTION = "-I 100 -N " + GMM_CLUSTERING_NUM + " -M 1.0E-6 -S 100";
        public static readonly string CALC_SIM_DIR = "CalcSimilarityOfUsers";
        public static readonly string LOGDENSITY = "logDensity";
        public static readonly string CAUCHY_SCHWARZ = "cauchySchwarz";
        public static readonly string MODEL_DIR = "models";
        public static readonly string HEIGHT_RESULT = "height_result";
        public static readonly string WEIGHT_RESULT = "weight_result";
        public static readonly string CSV_EXT = ".csv";
        public static readonly string BIN_EXT = ".bin";
        public static readonly string MODEL_EXT = ".model";
        // 特徴ベクトルの成分を何個用いるか
        public static readonly int USING_ATTRIBUTE_NUM = (int)(0.05 * 14 * 408);

        public static readonly List<string> RECOGNISED_ACTIVITIES = new List<string>() { "Standing", "Walking", "Running", "Sitting", "StairsUp", "StairsDown", "Bicycling", "BrushingTeeth", "WashingDishes", "UsingPC", "DrawingWhiteboard", "WritingNotebook", "PlayPingpong", "Vacuuming" };
        public static readonly string RECOGNISED_ARFF = "recognisedArff";


        MainManager mman;
        DataManagerForCube dman;
        readonly List<UserData> selectedUsers;
        // 1instance が一つのARFFファイルに対応。複数のARFFを読み込む場合は、instancesに含まれるinstanceも複数になる
        Dictionary<UserData, Instances> originalInstancesDict = new Dictionary<UserData, Instances>();
        // 行動認識した結果のinstancesを格納する
        Dictionary<UserData, Instances> recognisedInstancesDict = new Dictionary<UserData,Instances>();

        // コンストラクタ
        public CalcSimilarityOfUsers(MainManager mainManager, DataManagerForCube dataManager, List<UserData> selectedUsers) {
            if (mainManager == null || dataManager == null || selectedUsers == null) return;
            InitializeComponent();
            this.mman = mainManager;
            this.dman = dataManager;
            this.selectedUsers = selectedUsers;
            foreach (var selectedUser in selectedUsers) selectedUserListBox.Items.Add(selectedUser);
            // IDの順に並べる
            this.selectedUsers.Sort(new UserDataComparer());
            foreach (var activty in Enum.GetValues(typeof(Activity)) as Activity[]) activityComboBox.Items.Add(activty);

            numericalMethodsComboBox.ItemsSource = Utility.NumericMethodsDict.Keys;
        }

        #region 1. Manipurate Feature vectors using Correlation

        #region 1 export similarity between user's feature vector's element(attributes)

        private void exportSimilarityByEachElemButton_Click(object sender, RoutedEventArgs e) {
            var activity = (Activity)activityComboBox.SelectedItem;
            // GUIが固まるので、違うスレッドで
            Task.Factory.StartNew(() => {
                Utility.LoadOriginalArffFiles(dman.ArffDataRoot(), selectedUsers, ref originalInstancesDict);
                if (activity == Activity.AllActivities) {
                    Parallel.ForEach(Enum.GetValues(typeof(Activity)) as Activity[], (testingActivity) => {
                        if (testingActivity == Activity.AllActivities) return;
                        get1ActivityAllUsersAttributeDiffList(testingActivity);
                    });
                } else {
                    get1ActivityAllUsersAttributeDiffList(activity);
                }
            });
        }

        //　指定した行動における全ユーザの特徴の差やDcsなどを求める（メモリが4GBないと動かない）
        void get1ActivityAllUsersAttributeDiffList(Activity activity) {
            var gmmArrayDict = new Dictionary<UserData, GaussianMixtureModel[]>();
            #region すべてのユーザの準備を行う
            MainWindow.CW.Print(activity.ToString());
            foreach (var user in selectedUsers) {
                // ある行動のインスタンスを抽出する
                var extractedInstances = WekaUsefulMethods.ExtractInstancesByClass(originalInstancesDict[user], activity.ToString());
                var attributeArray = new double[ATTRIBUTE_NUM][];
                foreach (var attrIndex in Enumerable.Range(0, ATTRIBUTE_NUM)) attributeArray[attrIndex] = new double[extractedInstances.numInstances()];
                Instance instance;
                // 抽出しインスタンスの値を配列に放り込む
                foreach (int instanceIndex in Enumerable.Range(0, extractedInstances.numInstances())) {
                    instance = extractedInstances.instance(instanceIndex);
                    // 特徴ベクトルの各要素ごとにインスタンスからデータを抜き取る
                    foreach (int attrIndex in Enumerable.Range(0, ATTRIBUTE_NUM)) attributeArray[attrIndex][instanceIndex] = instance.value(attrIndex);
                }
                var gmmArray = new GaussianMixtureModel[ATTRIBUTE_NUM];
                // 特徴ベクトルの要素一つ一つで、GMMを作成する（ 配列への挿入はスレッドセーフ）
                Parallel.ForEach(Enumerable.Range(0, ATTRIBUTE_NUM), (attrIndex) => {
                    var gmmOfOneAttribute = new GaussianMixtureModel();
                    gmmOfOneAttribute.setOptions(Utils.splitOptions(CLUSTERING_OPTION));
                    gmmOfOneAttribute.buildClusterer(Utility.MakeInstances(attributeArray[attrIndex]));
                    gmmArray[attrIndex] = gmmOfOneAttribute;
                });
                gmmArrayDict.Add(user, gmmArray);
                MainWindow.CW.Print("Activity " + activity + " User " + user.ID + "  is ready");
            }

            #endregion
            if (gmmArrayDict != null) {
                #region 結果の出力
                var resultTupleList = new List<Tuple<int, int, TwoUsersComparedResult>>();
                foreach (var testUser in selectedUsers) {
                    var testUserIndex = selectedUsers.IndexOf(testUser);
                    foreach (var comparingUser in selectedUsers) {
                        var comparingUserIndex = selectedUsers.IndexOf(comparingUser);
                        // 同じユーザ、すでに比較したユーザを比較しないようにする
                        if (testUser != comparingUser && testUserIndex < comparingUserIndex) {
                            var result = get1Activity2UsersAttributesDiffList(testUser, comparingUser, activity, gmmArrayDict);
                            exportTwoUsersAttributesDiffHolder(result);
                            resultTupleList.Add(new Tuple<int, int, TwoUsersComparedResult>(testUserIndex, comparingUserIndex, result));
                        }
                    }
                }
                MainWindow.CW.Print("Finished " + activity);
                #endregion
            }
        }

        // 事前にgmmArrayListを用意してやるため、速い
        TwoUsersComparedResult get1Activity2UsersAttributesDiffList(UserData user1, UserData user2, Activity activity, Dictionary<UserData, GaussianMixtureModel[]> gmmArrayList) {
            TwoUsersComparedResult twoUsersComparedResult = null;
            int user1ID, user2ID;
            int.TryParse(user1.ID, out user1ID); int.TryParse(user2.ID, out user2ID);
            if (user1ID >= 0 && user2ID >= 0) {
                if (user1ID == user2ID) return null;
                if (user1ID > user2ID) {
                    Utility.Swap<UserData>(ref user1, ref user2);
                    Utility.Swap<int>(ref user1ID, ref user2ID);
                }
                double age1 = user1.numericattributes[0].value, age2 = user2.numericattributes[0].value;
                double height1 = user1.numericattributes[1].value, height2 = user2.numericattributes[1].value;
                double weight1 = user1.numericattributes[2].value, weight2 = user2.numericattributes[2].value;
                var user1Attr = new List<double>() { age1, height1, weight1 };
                var user2Attr = new List<double>() { age2, height2, weight2 };
                twoUsersComparedResult = new TwoUsersComparedResult(activity.ToString(), user1ID, user2ID, user1Attr, user2Attr);
            }
            if (twoUsersComparedResult == null) return null;
            var dcsList = new List<double>(ATTRIBUTE_NUM);
            CauchySchwarzDivGMM cauchySchwarzDiv;
            //　408要素のDcsを求める
            for (int attrIndex = 0; attrIndex < ATTRIBUTE_NUM; attrIndex++) {
                cauchySchwarzDiv = new CauchySchwarzDivGMM(gmmArrayList[user1][attrIndex], gmmArrayList[user2][attrIndex]);
                dcsList.Add(cauchySchwarzDiv.GetDcsResult());
            }
            twoUsersComparedResult.DcsList = dcsList;
            return twoUsersComparedResult;
        }

        void exportTwoUsersAttributesDiffHolder(TwoUsersComparedResult data) {
            if (data == null) return;
            var processFolderPath = dman.cdman.rootpath;
            Directory.CreateDirectory(Path.Combine(processFolderPath, CORRELATION_DIRNAME, data.Activity));
            var filePathArray = new string[] { processFolderPath, CORRELATION_DIRNAME, data.Activity, data.MakeFileName() };
            //保存先のファイル名
            string fileName = Path.Combine(filePathArray);
            var serializer = new XmlSerializer(typeof(TwoUsersComparedResult));
            using (var fs = new FileStream(fileName, FileMode.Create)) {
                serializer.Serialize(fs, data);
            }
        }

        #endregion

        #region 2 get correlation using user's attributes similarity
        // 相関を求めて、使わない特徴ベクトルの成分を見つけて出力する
        private void removeAttributesButton_Click(object sender, RoutedEventArgs e) {
            var selectedDirPath = Utility.SelectFolderPath(dman.cdman.rootpath);
            if (selectedDirPath == null) return;
            var activityDirs = Directory.GetDirectories(selectedDirPath);
            Task.Factory.StartNew(() => {
                MainWindow.CW.Print("Start Correlation Thread " + Thread.CurrentThread.ManagedThreadId);
                // 各ユーザを抜いた状態で身体的特徴と類似度の相関を求めて、低い相関を持つattribute(特徴ベクトルの成分)を保存する
                var ageUsingAttributeDict = new Dictionary<UserData, Dictionary<string, List<Tuple<int, double>>>>();
                var ageNegativeAttributeDict = new Dictionary<UserData, Dictionary<string, List<Tuple<int, double>>>>();
                var heightUsingAttributeDict = new Dictionary<UserData, Dictionary<string, List<Tuple<int, double>>>>();
                var heightNegativeAttributeDict = new Dictionary<UserData, Dictionary<string, List<Tuple<int, double>>>>();
                var weightUsingAttributeDict = new Dictionary<UserData, Dictionary<string, List<Tuple<int, double>>>>();
                var weightNegativeAttributeDict = new Dictionary<UserData, Dictionary<string, List<Tuple<int, double>>>>();
                var resultTupleList = new Dictionary<string, List<Tuple<int, int, TwoUsersComparedResult>>>();
                // 500MB取り出すため、メモリが死ぬかも
                foreach (var activityDirPath in activityDirs) {
                    var activityStr = Path.GetFileName(activityDirPath);
                    if (activityStr == MainWindow.RESULT_DIR) continue;
                    resultTupleList[activityStr] = loadListOfOneActivitySimilarityFiles(activityDirPath);
                }
                MainWindow.CW.Print(" Load Finished ");

                #region 画像として出力（どんな相関なのかを表示しなければならない）

                #endregion

                foreach (var testingUser in selectedUsers) {
                    var age1UserCorResult = new Dictionary<string, double[]>();
                    var height1UserCorResult = new Dictionary<string, double[]>();
                    var weight1UserCorResult = new Dictionary<string, double[]>();
                    foreach (var activity in Enum.GetValues(typeof(Activity)) as Activity[]) {
                        if (activity == Activity.AllActivities) continue;
                        var activityStr = activity.ToString();
                        var result = getCorrelationResult(resultTupleList[activityStr], testingUser);
                        age1UserCorResult.Add(activityStr, result.Item1);
                        height1UserCorResult.Add(activityStr, result.Item2);
                        weight1UserCorResult.Add(activityStr, result.Item3);
                    }
                    // 一人のユーザを抜いた場合の相関が求まったので、必要のない特徴ベクトルの成分を見つける
                    var age1UserUsingAttributeDict = new Dictionary<string, List<Tuple<int, double>>>();
                    var age1UserNegativeAttributeDict = new Dictionary<string, List<Tuple<int, double>>>();
                    filterAttributes(age1UserCorResult, ref age1UserUsingAttributeDict, ref age1UserNegativeAttributeDict);
                    ageUsingAttributeDict[testingUser] = age1UserUsingAttributeDict;
                    ageNegativeAttributeDict[testingUser] = age1UserNegativeAttributeDict;
                    var height1UserUsingAttributeDict = new Dictionary<string, List<Tuple<int, double>>>();
                    var height1UserNegativeAttributeDict = new Dictionary<string, List<Tuple<int, double>>>();
                    filterAttributes(height1UserCorResult, ref height1UserUsingAttributeDict, ref height1UserNegativeAttributeDict);
                    heightUsingAttributeDict[testingUser] = height1UserUsingAttributeDict;
                    heightNegativeAttributeDict[testingUser] = height1UserNegativeAttributeDict;
                    var weight1UserUsingAttributeDict = new Dictionary<string, List<Tuple<int, double>>>();
                    var weight1UserNegativeAttributeDict = new Dictionary<string, List<Tuple<int, double>>>();
                    filterAttributes(weight1UserCorResult, ref weight1UserUsingAttributeDict, ref weight1UserNegativeAttributeDict);
                    weightUsingAttributeDict[testingUser] = weight1UserUsingAttributeDict;
                    weightNegativeAttributeDict[testingUser] = weight1UserNegativeAttributeDict;
                }
                MainWindow.CW.Print("Finish Calculation of Correlation");

                #region 相関から得た必要な成分、負の相関をもつ成分の出力
                Directory.CreateDirectory(Path.Combine(selectedDirPath, MainWindow.RESULT_DIR, USING_ATTRIBUTES, "age"));
                Directory.CreateDirectory(Path.Combine(selectedDirPath, MainWindow.RESULT_DIR, USING_ATTRIBUTES_RANKING, "age"));
                Directory.CreateDirectory(Path.Combine(selectedDirPath, MainWindow.RESULT_DIR, USING_ATTRIBUTES, "height"));
                Directory.CreateDirectory(Path.Combine(selectedDirPath, MainWindow.RESULT_DIR, USING_ATTRIBUTES_RANKING, "height"));
                Directory.CreateDirectory(Path.Combine(selectedDirPath, MainWindow.RESULT_DIR, USING_ATTRIBUTES, "weight"));
                Directory.CreateDirectory(Path.Combine(selectedDirPath, MainWindow.RESULT_DIR, USING_ATTRIBUTES_RANKING, "weight"));
                foreach (var user in selectedUsers) {
                    exportUsingAttributesAndRanking(selectedDirPath, "age", user, ageUsingAttributeDict, ageNegativeAttributeDict);
                    exportUsingAttributesAndRanking(selectedDirPath, "height", user, heightUsingAttributeDict, heightNegativeAttributeDict);
                    exportUsingAttributesAndRanking(selectedDirPath, "weight", user, weightUsingAttributeDict, weightNegativeAttributeDict);
                }
                #endregion
                MainWindow.CW.Print(" Finished Correlation");
            });
        }

        // 出力したGMM類似度を計算したxmlファイルをロードする
        List<Tuple<int, int, TwoUsersComparedResult>> loadListOfOneActivitySimilarityFiles(string activityDirPath) {
            var serializer = new XmlSerializer(typeof(TwoUsersComparedResult));
            var xmlFiles = Directory.GetFiles(activityDirPath, "*.xml");
            var resultTupleList = new List<Tuple<int, int, TwoUsersComparedResult>>();
            if (xmlFiles == null || xmlFiles.Length == 0) return null;
            var ageCorrelationDict = new Dictionary<string, double[]>();
            var heightCorrelationDict = new Dictionary<string, double[]>();
            var weightCorrelationDict = new Dictionary<string, double[]>();

            foreach (var xmlFilePath in xmlFiles) {
                var resultTuple = serializer.Deserialize(new FileStream(xmlFilePath, FileMode.Open)) as TwoUsersComparedResult;
                // xmlを読み込んで、でシリアライズし、resultTupleListに格納
                resultTupleList.Add(new Tuple<int, int, TwoUsersComparedResult>(resultTuple.UserID1, resultTuple.UserID2, resultTuple));
            }
            return resultTupleList;
        }

        //　一つの行動における相関を求める（引数のremovingUserは相関を求めるときに使わないユーザ）
        Tuple<double[], double[], double[]> getCorrelationResult(List<Tuple<int, int, TwoUsersComparedResult>> allResultTupleList, UserData removingUser) {
            var resultTupleList = allResultTupleList;
            if (removingUser != null) {
                int removingUserID;
                if (int.TryParse(removingUser.ID, out removingUserID) == false) return null;
                resultTupleList = (from tuple in allResultTupleList where tuple.Item1 != removingUserID && tuple.Item2 != removingUserID select tuple).ToList();
            }
            var ageDiffList = new double[resultTupleList.Count];
            var heightDiffList = new double[resultTupleList.Count];
            var weightDiffList = new double[resultTupleList.Count];
            var dcsListArrary = new double[ATTRIBUTE_NUM][];
            foreach (int attrIndex in Enumerable.Range(0, ATTRIBUTE_NUM)) dcsListArrary[attrIndex] = new double[resultTupleList.Count];
            // 身体的特徴の差を格納
            foreach (int index in Enumerable.Range(0, resultTupleList.Count)) {
                var dataTuple = resultTupleList[index];
                ageDiffList[index] = dataTuple.Item3.UserDiffAttributes[0];
                heightDiffList[index] = dataTuple.Item3.UserDiffAttributes[1];
                weightDiffList[index] = dataTuple.Item3.UserDiffAttributes[2];
                foreach (int attrIndex in Enumerable.Range(0, ATTRIBUTE_NUM))
                    dcsListArrary[attrIndex][index] = dataTuple.Item3.DcsList[attrIndex];
            }
            var ageCorrelations = new double[ATTRIBUTE_NUM];
            var heightCorrelations = new double[ATTRIBUTE_NUM];
            var weightCorrelations = new double[ATTRIBUTE_NUM];
            // 特徴ベクトルの各要素ごとに相関を求める
            foreach (var attrIndex in Enumerable.Range(0, ATTRIBUTE_NUM)) {
                ageCorrelations[attrIndex] = calculateCorrelation(ageDiffList, dcsListArrary[attrIndex]);
                heightCorrelations[attrIndex] = calculateCorrelation(heightDiffList, dcsListArrary[attrIndex]);
                weightCorrelations[attrIndex] = calculateCorrelation(weightDiffList, dcsListArrary[attrIndex]);
            }
            return new Tuple<double[], double[], double[]>(ageCorrelations, heightCorrelations, weightCorrelations);
        }

        // すべてのユーザの身体的特徴の差とDcsを渡すと、相関を得る
        double calculateCorrelation(double[] userAttributeDiffList, double[] dcsList) {
            if (userAttributeDiffList.Length != dcsList.Length) return double.MinValue;
            var userAttributeDiffAverage = userAttributeDiffList.Average();
            var dcsAverage = dcsList.Average();
            double Sxx = 0, Sxy = 0, Syy = 0;
            foreach (var index in Enumerable.Range(0, userAttributeDiffList.Length)) {
                var userAttrDiff = userAttributeDiffList[index] - userAttributeDiffAverage;
                var dcsDiff = dcsList[index] - dcsAverage;
                Sxx += userAttrDiff * userAttrDiff;
                Sxy += userAttrDiff * dcsDiff;
                Syy += dcsDiff * dcsDiff;
            }
            return Sxy / (Math.Sqrt(Sxx) * Math.Sqrt(Syy));
        }

        // 次元の削減率を決めて、それに応じた成分を削除する
        void filterAttributes(Dictionary<string, double[]> oneUserCorResult, ref Dictionary<string, List<Tuple<int, double>>> usingAttributeDict,
                                                                                                                    ref Dictionary<string, List<Tuple<int, double>>> negativeAttributeDict) {
            // 行動ごとではなく、全体で順番を調べ、相関の高いものから並べる
            var descendingCorrelations = oneUserCorResult.SelectMany(x => x.Value).OrderByDescending(x => Math.Abs(x)).ToList();
            var threshold = Math.Abs(descendingCorrelations[USING_ATTRIBUTE_NUM - 1]);
            foreach (var key in oneUserCorResult.Keys) {
                usingAttributeDict[key] = new List<Tuple<int, double>>();
                negativeAttributeDict[key] = new List<Tuple<int, double>>();
            }
            int addedAttributeNum = 0;
            foreach (var keyvalue in oneUserCorResult) {
                for (int attrIndex = 0; attrIndex < ATTRIBUTE_NUM; attrIndex++) {
                    // USING_ATTRIBUTE_NUM だけ成分を求めたので、残りはいらない（閾値と等しい値が複数あった場合に、数を制限する）
                    if (addedAttributeNum >= USING_ATTRIBUTE_NUM) return;
                    var correlation = keyvalue.Value[attrIndex];
                    if (Math.Abs(correlation) >= threshold) {
                        usingAttributeDict[keyvalue.Key].Add(new Tuple<int, double>(attrIndex, correlation));
                        if (correlation < 0) negativeAttributeDict[keyvalue.Key].Add(new Tuple<int, double>(attrIndex, correlation));
                        addedAttributeNum++;
                    }
                }
            }
        }

        void exportUsingAttributesAndRanking(string selectedDirPath, string physCharStr, UserData user,
            Dictionary<UserData, Dictionary<string, List<Tuple<int, double>>>> usingAttributeDict, Dictionary<UserData, Dictionary<string, List<Tuple<int, double>>>> negativeAttributeDict) {
            var usingAttrFileName = user.ID + "_" + USING_ATTRIBUTES + CSV_EXT;
            var usingAttrExportPath = Path.Combine(selectedDirPath, MainWindow.RESULT_DIR, USING_ATTRIBUTES, physCharStr, usingAttrFileName);
            using (var sw = new StreamWriter(usingAttrExportPath)) {
                // 行動と成分のインデックスを出力
                foreach (var keyvalue in usingAttributeDict[user].OrderBy(keyvalue => keyvalue.Key)) {
                    sw.Write(keyvalue.Key + ",");
                    sw.WriteLine(string.Join(",", keyvalue.Value.Select(tuple => tuple.Item1)));
                }
                sw.WriteLine(NEGATIVE_CORRELATION_ATTRIBUTES);
                foreach (var keyvalue in negativeAttributeDict[user].OrderBy(keyvalue => keyvalue.Key)) {
                    sw.Write(keyvalue.Key + ",");
                    sw.WriteLine(string.Join(",", keyvalue.Value.Select(tuple => tuple.Item1)));
                }
            }
            var usingAttrRankingFileName = user.ID + "_" + USING_ATTRIBUTES_RANKING + CSV_EXT;
            var usingAttrRankingExportPath = Path.Combine(selectedDirPath, MainWindow.RESULT_DIR, USING_ATTRIBUTES_RANKING, physCharStr, usingAttrRankingFileName);
            var userAttrRankingList = createUsingAttributeRanking(usingAttributeDict[user]);
            using (var sw = new StreamWriter(usingAttrRankingExportPath))
                foreach (var t in userAttrRankingList)
                    sw.WriteLine(new StringBuilder(t.Item1).Append(",").Append(t.Item2).Append(",").Append(t.Item3));
        }

        List<Tuple<string, int, double>> createUsingAttributeRanking(Dictionary<string, List<Tuple<int, double>>> oneUserUsingAttributeDict) {
            // 行動毎に相関の順番で並べる
            var orderedUsingAttributes = new Dictionary<string, List<Tuple<int, double>>>();
            foreach (var keyValue in oneUserUsingAttributeDict)
                orderedUsingAttributes[keyValue.Key] = keyValue.Value.OrderBy(tuple => tuple.Item2).ToList();

            var rankingList = new List<Tuple<string, int, double>>();
            foreach (var key in orderedUsingAttributes.Keys)
                foreach (var tuple in orderedUsingAttributes[key])
                    rankingList.Add(new Tuple<string, int, double>(key, tuple.Item1, tuple.Item2));
            return rankingList.OrderByDescending(t => t.Item3).ToList();
        }

        #endregion

        #region 3 相関が高いattributeを用いて、身体的特徴を推定する

        private void estimateAttributesFromCorrelationButton_Click(object sender, RoutedEventArgs e) {
            var correlationRootPath = Utility.SelectFolderPath(dman.cdman.rootpath);
            if (correlationRootPath == null) return;
            var activityDirs = Directory.GetDirectories(correlationRootPath);
            var physicalChar = getCheckedUserPhysicalChar();
            Task.Factory.StartNew(() => {
                MainWindow.CW.Print("Start Loading Data Thread " + Thread.CurrentThread.ManagedThreadId);
                var similarityDict = new Dictionary<string, Dictionary<int, Dictionary<int, TwoUsersComparedResult>>>();
                // 500MB取り出すため、メモリが死ぬかも
                foreach (var activityDirPath in activityDirs) {
                    var activityStr = Path.GetFileName(activityDirPath);
                    if (activityStr == MainWindow.RESULT_DIR) continue;
                    similarityDict[activityStr] = loadDictOfOneActivitySimilarityFiles(activityDirPath);
                }
                // 相関のランキングファイルのインポート
                var usingAttributeRankingList = loadUsingAttributeRankingList(correlationRootPath, physicalChar);
                if (usingAttributeRankingList.Count == 0) return;

                MainWindow.CW.Print("Finished Loading");

                var resultRootPath = Path.Combine(correlationRootPath, MainWindow.RESULT_DIR);
                var exportRankingPath = Path.Combine(resultRootPath, MainWindow.RANKING, physicalChar.ToString());
                Directory.CreateDirectory(exportRankingPath);
                var exportInferredResultPath = Path.Combine(resultRootPath, MainWindow.INFERRED_RESULT, physicalChar.ToString());
                Directory.CreateDirectory(exportInferredResultPath);

                // 要らない属性を消し、必要な属性において、類似度のもっとも高いユーザを列挙
                foreach (var testUser in selectedUsers) {
                    // int -> 成分のインデックス、string -> 比較した相手のID, double -> 類似度
                    var dcsTupleList = new List<Tuple<string, int, List<Tuple<string, double>>>>();
                    int testUserID, comparingUserID;
                    if (int.TryParse(testUser.ID, out testUserID) == false) continue;
                    foreach (var rankingTuple in usingAttributeRankingList[testUser]) {
                        var activity = rankingTuple.Item1; var attrIdx = rankingTuple.Item2;
                        dcsTupleList.Add(new Tuple<string, int, List<Tuple<string, double>>>(activity, attrIdx, new List<Tuple<string, double>>()));
                        // テストユーザ以外のユーザのうち、類似度が高いものを求める　
                        foreach (var comparingUser in selectedUsers) {
                            // test userは除く
                            if (comparingUser == testUser) continue;
                            if (int.TryParse(comparingUser.ID, out comparingUserID) == false) continue;
                            var twoUsersResult = similarityDict[activity][testUserID][comparingUserID];
                            dcsTupleList.Last().Item3.Add(new Tuple<string, double>(comparingUser.ID, twoUsersResult.DcsList[attrIdx]));
                        }
                    }
                    // Dcsが小さい順に並び替え
                    for (int i = 0; i < dcsTupleList.Count; i++) {
                        var tuple = dcsTupleList[i];
                        dcsTupleList[i] = new Tuple<string, int, List<Tuple<string, double>>>(tuple.Item1, tuple.Item2, tuple.Item3.OrderBy(t => t.Item2).ToList());
                    }
                    exportUserRankingAndInferredResult(exportRankingPath, exportInferredResultPath, physicalChar, testUser, dcsTupleList);
                }
                MainWindow.CW.Print(" Finished Exporting");
            });
        }

        // 出力したGMM類似度を計算したxmlファイルをロードする
        Dictionary<int, Dictionary<int, TwoUsersComparedResult>> loadDictOfOneActivitySimilarityFiles(string activityDirPath) {
            var serializer = new XmlSerializer(typeof(TwoUsersComparedResult));
            var xmlFiles = Directory.GetFiles(activityDirPath, "*.xml");
            var similarityDict = new Dictionary<int, Dictionary<int, TwoUsersComparedResult>>();
            if (xmlFiles == null || xmlFiles.Length == 0) return null;
            var ageCorrelationDict = new Dictionary<string, double[]>();
            var heightCorrelationDict = new Dictionary<string, double[]>();
            var weightCorrelationDict = new Dictionary<string, double[]>();

            foreach (var xmlFilePath in xmlFiles) {
                var twoUsersComparedResult = serializer.Deserialize(new FileStream(xmlFilePath, FileMode.Open)) as TwoUsersComparedResult;
                // xmlを読み込んで、でシリアライズし、resultTupleListに格納
                if (similarityDict.ContainsKey(twoUsersComparedResult.UserID1) == false) similarityDict.Add(twoUsersComparedResult.UserID1, new Dictionary<int, TwoUsersComparedResult>());
                if (similarityDict.ContainsKey(twoUsersComparedResult.UserID2) == false) similarityDict.Add(twoUsersComparedResult.UserID2, new Dictionary<int, TwoUsersComparedResult>());
                similarityDict[twoUsersComparedResult.UserID1].Add(twoUsersComparedResult.UserID2, twoUsersComparedResult);
                similarityDict[twoUsersComparedResult.UserID2].Add(twoUsersComparedResult.UserID1, twoUsersComparedResult);
            }
            return similarityDict;
        }

        // 相関のファイルのインポート
        Dictionary<UserData, List<Tuple<string, int>>> loadUsingAttributeRankingList(string correlationRootPath, PhysicalChar physicalChar) {
            var usingAttributeRankingDict = new Dictionary<UserData, List<Tuple<string, int>>>();
            var correlationFiles = Directory.GetFiles(Path.Combine(correlationRootPath, MainWindow.RESULT_DIR, USING_ATTRIBUTES_RANKING, physicalChar.ToString()));
            foreach (var corFile in correlationFiles) {
                // ファイル名につけたuser のIDを取得する
                var corFileName = Path.GetFileName(corFile);
                var userID = corFileName.Substring(0, 4);
                var user = MainWindow.UserIDDict[userID];
                if (user == null) continue;
                usingAttributeRankingDict.Add(user, new List<Tuple<string, int>>());
                using (var sr = new StreamReader(corFile)) {
                    string[] splitOneLine; int attrIdx;
                    while (sr.Peek() >= 0) {
                        splitOneLine = sr.ReadLine().Split(',');
                        var activity = splitOneLine[0];
                        if (int.TryParse(splitOneLine[1], out attrIdx)) usingAttributeRankingDict[user].Add(new Tuple<string, int>(activity, attrIdx));
                    }
                }
            }
            return usingAttributeRankingDict;
        }

        void exportUserRankingAndInferredResult(string exportRankingPath, string exportInferredResultPath, PhysicalChar physicalChar, UserData testUser,
                                                                            List<Tuple<string, int, List<Tuple<string, double>>>> dcsTupleDict) {
            var fileName = testUser.ID + CSV_EXT;
            // 0番目に行動とその成分のインデックス、それ以降はユーザのID
            using (var sw = new StreamWriter(Path.Combine(exportRankingPath, fileName)))
                foreach (var tuple in dcsTupleDict) sw.WriteLine(tuple.Item1 + "_" + tuple.Item2 + "," + string.Join(",", tuple.Item3.Select(t => t.Item1)));

            var attributeData = new double[dcsTupleDict.Count][];
            for (int allAttrIdx = 0; allAttrIdx < dcsTupleDict.Count; allAttrIdx++) {
                attributeData[allAttrIdx] = new double[MainWindow.USER_NUM - 1];
                for (int userIdx = 0; userIdx < MainWindow.USER_NUM - 1; userIdx++) {
                    var user = MainWindow.UserIDDict[dcsTupleDict[allAttrIdx].Item3[userIdx].Item1];
                    attributeData[allAttrIdx][userIdx] = Utility.GetNumericalPhysicalCharData(user, physicalChar);
                }
            }
            var inferredResult = new double[top_num / 2 + 1][];
            //foreach (var i in Enumerable.Range(1, top_num).Where(i => i % 2 == 1))
            //    inferredResult[i / 2] = MainWindow.AverageOfTopN(attributeData, 0, i);
            // ユーザの身体的特徴も出しておく
            using (var sw = new StreamWriter(Path.Combine(exportInferredResultPath, fileName)))
                foreach (var inferredResultArray in inferredResult) sw.WriteLine(string.Join(",", inferredResultArray));
        }

        #endregion

        #region  4 高度な分析

        private void sophisticatedAnalysisButton_Click(object sender, RoutedEventArgs e) {
            var correlationRootPath = Utility.SelectFolderPath(dman.cdman.rootpath);
            if (correlationRootPath == null) return;
            var setting = new SophisticatedAnalysisSetting() {
                IsLabeled = true, Activity = Activity.AllActivities, Classifier = getClassifier(),
                PhysicalChar = getCheckedUserPhysicalChar(),
                UsePCA = pcaCheckBox.IsChecked.Value,
                UseResample = resampleCheckBox.IsChecked.Value
            };
            var physicalChar = getCheckedUserPhysicalChar();
            // 高度な分析のための辞書
            var userInstances = (Directory.Exists(Path.Combine(correlationRootPath, MainWindow.RESULT_DIR, "arff", physicalChar.ToString()))) ?
                                            readArff(correlationRootPath, physicalChar) : createArff(correlationRootPath, physicalChar);
            var classifier = getClassifier();
            // TODO:ここの箇所は間違っている
            var method = new SophisticatedAnalysisMethod(mman, dman, selectedUsers, setting);
            method.AllInstancesDict = userInstances;
            method.ExecWekaLearnMethod(true, true, true, 5);
        }

        Dictionary<UserData, Instances> createArff(string correlationRootPath, PhysicalChar physicalChar) {
            var totalAttrNum = USING_ATTRIBUTE_NUM * (top_num / 2 + 1) + 1;
            var userDirs = Directory.GetDirectories(correlationRootPath);

            #region 定義の作成
            var attrVector = new FastVector(totalAttrNum);
            foreach (var i in Enumerable.Range(1, top_num).Where(i => i % 2 == 1)) {
                var topStr = "top_" + i + "_";
                foreach (var rank in Enumerable.Range(0, USING_ATTRIBUTE_NUM))
                    attrVector.addElement(new weka.core.Attribute(topStr + rank.ToString()));
            }
            attrVector.addElement(new weka.core.Attribute("class"));
            #endregion

            var userInstances = new Dictionary<UserData, Instances>();
            #region クラス（答え）の設定
            foreach (var user in selectedUsers) {
                var instances = new Instances("Relation", attrVector, 1);
                instances.setClassIndex(totalAttrNum - 1);
                var instance = new Instance(totalAttrNum);
                instance.setValue(instances.attribute(totalAttrNum - 1), Utility.GetNumericalPhysicalCharData(user, physicalChar));
                instances.add(instance);
                userInstances.Add(user, instances);
            }
            #endregion

            #region データの代入
            var inferredResultFiles = Directory.GetFiles(Path.Combine(correlationRootPath, MainWindow.RESULT_DIR, MainWindow.INFERRED_RESULT, physicalChar.ToString()));
            foreach (var inferredResultFile in inferredResultFiles) {
                var userIDStr = Path.GetFileNameWithoutExtension(inferredResultFile);
                var testUser = MainWindow.UserIDDict[userIDStr];
                var inferredData = new string[top_num / 2 + 1][];
                using (var sr = new StreamReader(inferredResultFile)) {
                    int row = 0;
                    while (sr.Peek() >= 0) {
                        inferredData[row] = sr.ReadLine().Split(',');
                        row++;
                    }
                }
                double value;
                for (int row = 0; row < (top_num / 2 + 1); row++)
                    for (int rank = 0; rank < USING_ATTRIBUTE_NUM; rank++)
                        if (double.TryParse(inferredData[row][rank], out value))
                            userInstances[testUser].instance(0).setValue(rank + row * USING_ATTRIBUTE_NUM, value);
            }
            #endregion
            foreach (var keyValue in userInstances) {
                var arffPath = Path.Combine(correlationRootPath, MainWindow.RESULT_DIR, "arff", physicalChar.ToString(), keyValue.Key.ID + ".arff");
                WekaUsefulMethods.SaveArff(keyValue.Value, arffPath);
            }
            return userInstances;
        }

        Dictionary<UserData, Instances> readArff(string correlationRootPath, PhysicalChar physicalChar) {
            var userInstances = new Dictionary<UserData, Instances>();
            var arffFiles = Directory.GetFiles(Path.Combine(correlationRootPath, MainWindow.RESULT_DIR, "arff", physicalChar.ToString()), "*.arff");
            foreach (var arffFile in arffFiles) {
                var userIDStr = Path.GetFileNameWithoutExtension(arffFile);
                var user = MainWindow.UserIDDict[userIDStr];
                var instances = WekaUsefulMethods.LoadArffFile(arffFile);
                instances.setClassIndex(instances.numAttributes() - 1);
                userInstances[user] = instances;
            }
            return userInstances;
        }

        WekaConstantParams.WekaClassifiers getClassifier() {
            return Utility.GetClassifier(numericalMethodsComboBox.SelectedItem as string, false);
        }

        #endregion

        #region TwoUsersComparedResult Class
        public class TwoUsersComparedResult {
            [XmlElement("Activity")]
            public string Activity { get; set; }
            [XmlElement("UserID1")]
            public int UserID1 { get; set; }
            [XmlElement("User1Attributes")]
            public List<double> User1Attributes { get; set; }
            [XmlElement("UserID2")]
            public int UserID2 { get; set; }
            [XmlElement("User2Attributes")]
            public List<double> User2Attributes { get; set; }
            [XmlElement("UserDiffAttributes")]
            public List<double> UserDiffAttributes { get; set; }
            [XmlArrayItemAttribute("User1ParameterAttributes")]
            public List<List<double>> User1ParameterAttributes { get; set; }
            [XmlArrayItemAttribute("User2ParameterAttributes")]
            public List<List<double>> User2ParameterAttributes { get; set; }
            // CauchySchwarzDivergenceのリスト
            [XmlElement("DcsList")]
            public List<double> DcsList { get; set; }

            public TwoUsersComparedResult() { }

            public TwoUsersComparedResult(string activity, int userID1, int userID2, List<double> user1Attr, List<double> user2Attr) {
                if (user1Attr.Count == user2Attr.Count) {
                    Activity = activity; UserID1 = userID1; UserID2 = userID2;
                    User1Attributes = user1Attr; User2Attributes = user2Attr;
                    UserDiffAttributes = new List<double>() { Math.Abs(user1Attr[0] - user2Attr[0]), Math.Abs(user1Attr[1] - user2Attr[1]), Math.Abs(user1Attr[2] - user2Attr[2]) };
                }
            }
            public string MakeFileName() {
                return new StringBuilder(Activity).Append("_userID1_").Append(UserID1).Append("_userID2_").Append(UserID2)
                    .Append("_ARFF_").Append(Utility.READ_ARFF_NUMBER).Append("_CLUSTER_").Append(GMM_CLUSTERING_NUM)
                        .Append(XML_EXT).ToString();
            }
        }
        #endregion

        #endregion

        #region 2. Calculate  Similarity Between each users
        // （相関を用いない場合）
        private void startComparingButton_Click(object sender, RoutedEventArgs e) {
            startComparing();
        }

        // （相関を用いる場合）相関のファイルをインポートして、特徴ベクトルの成分数を減らす
        private void useCorrelationButton_Click(object sender, RoutedEventArgs e) {
            var correlationRootPath = Utility.SelectFolderPath(dman.cdman.rootpath);
            if (correlationRootPath == null) return;
            // 相関のファイルのインポート
            Dictionary<UserData, Dictionary<string, List<string>>> removingAttributeDict;
            Dictionary<UserData, Dictionary<string, List<string>>> negativeAttributeDict;
            var physicalChar = getCheckedUserPhysicalChar();
            loadUsingAttributeDict(correlationRootPath, physicalChar, out removingAttributeDict, out negativeAttributeDict);
            if (removingAttributeDict == null || removingAttributeDict.Count == 0) {
                MainWindow.CW.Print("Something wrong with removingAttributeList", ErrorStatus.Error);
                return;
            }
            startComparing(removingAttributeDict, physicalChar);
        }
        // 相関のファイルのインポート
        void loadUsingAttributeDict(string correlationRootPath, PhysicalChar physicalChar,
            out Dictionary<UserData, Dictionary<string, List<string>>> removingAttributesDict,
            out Dictionary<UserData, Dictionary<string, List<string>>> negativeAttributeDict) {
            removingAttributesDict = new Dictionary<UserData, Dictionary<string, List<string>>>();
            negativeAttributeDict = new Dictionary<UserData, Dictionary<string, List<string>>>();
            var correlationFiles = Directory.GetFiles(Path.Combine(correlationRootPath, MainWindow.RESULT_DIR, USING_ATTRIBUTES, physicalChar.ToString()));
            foreach (var corFile in correlationFiles) {
                // ファイル名につけたuser のIDを取得する
                var corFileName = Path.GetFileName(corFile);
                var userID = corFileName.Substring(0, 4);
                var testUser = MainWindow.UserIDDict[userID];
                var usingAttributesDict = new Dictionary<string, List<int>>();
                removingAttributesDict.Add(testUser, new Dictionary<string, List<string>>());
                negativeAttributeDict.Add(testUser, new Dictionary<string, List<string>>());
                // 負の相関を持つ成分を読み込むためのスイッチ
                bool changeDict = false;
                using (var sr = new StreamReader(corFile)) {
                    string[] splitOneLine;
                    int[] intSplitOneLine;
                    while (sr.Peek() >= 0) {
                        splitOneLine = sr.ReadLine().Split(',');
                        var activity = splitOneLine[0];
                        intSplitOneLine = Utility.ConvertStrArrayToIntArray(splitOneLine);
                        if (activity == NEGATIVE_CORRELATION_ATTRIBUTES) {
                            changeDict = true;
                            continue;
                        }
                        if (changeDict == false) {
                            usingAttributesDict[activity] = new List<int>();
                            for (int col = 1; col < splitOneLine.Length; col++) usingAttributesDict[activity].Add(intSplitOneLine[col]);
                        } else {
                            negativeAttributeDict[testUser][activity] = new List<string>();
                            for (int col = 1; col < splitOneLine.Length; col++)
                                if (intSplitOneLine[col] != -1) negativeAttributeDict[testUser][activity].Add(ATTRIBUTE_LIST[intSplitOneLine[col]]);
                        }
                    }
                }
                // 0-407のリストを作り、usingAttributesとの差集合をとることで、removingAttributesを得る
                var intList = Enumerable.Range(0, ATTRIBUTE_NUM);
                foreach (var keyvalue in usingAttributesDict) {
                    var removingList = from attrIdx in intList.Except(keyvalue.Value) select ATTRIBUTE_LIST[attrIdx];
                    removingAttributesDict[testUser][keyvalue.Key] = removingList.ToList();
                }
            }
        }

        void startComparing(Dictionary<UserData, Dictionary<string, List<string>>> removingAttributeDict = null, PhysicalChar? physicalChar = null) {
            var ts = new TestSetting() {
                ProcessDirPath = dman.cdman.rootpath,
                IsNormalMethod = (removingAttributeDict == null),
                IsLabeled = labeledDataRadioButton.IsChecked.Value,
                IsLogDensity = logDensityRadioButton.IsChecked.Value,
                Activity = (Activity)activityComboBox.SelectedItem
            };
            var recognisedArffRootPath = recognisedDataRootPathTextBlock.Text;
            Task.Factory.StartNew(() => {
                #region 行動認識した結果を取り入れたarffを読み込ませる
                if (ts.IsLabeled == false) { // 行動認識の場合
                    if (Directory.Exists(recognisedArffRootPath) == false) {
                        MainWindow.CW.Print("Recognised Dir must be set", ErrorStatus.Error);
                        return;
                    } else {
                        Utility.LoadRecognisedArffFiles(recognisedArffRootPath, selectedUsers, ref recognisedInstancesDict);
                    }
                }
                #endregion
                // 元となる特徴ベクトルデータを取得
                Utility.LoadOriginalArffFiles(dman.ArffDataRoot(), selectedUsers, ref originalInstancesDict);
                // 一つの行動に対し、すべてのセンサの組み合わせをテストする
                if (ts.Activity == Activity.AllActivities) {
                    foreach (var activity in Enum.GetValues(typeof(Activity)) as Activity[]) {
                        if (activity == Activity.AllActivities) continue;
                        #region 0001から1111までで必要そうなセンサーで比較を行う
                        foreach (var sensorIdx in Utility.AllSensorPatternIndices) {
                            var tsWithActivitySensorSetting = ts; // 構造体をクローン
                            tsWithActivitySensorSetting.UsedSensors = Utility.MakeFlags(sensorIdx);
                            tsWithActivitySensorSetting.Activity = activity;
                            if (tsWithActivitySensorSetting.IsNormalMethod)
                                compareBy1Activity1SensorPattern(tsWithActivitySensorSetting);
                            else
                                compareBy1Activity1SensorPattern(tsWithActivitySensorSetting, removingAttributeDict, physicalChar);
                        }
                        #endregion
                    }
                } else {
                    #region 0001から1111までで必要そうなセンサーで比較を行う
                    foreach (var sensorIdx in Utility.AllSensorPatternIndices) {
                        var tsWithSensorSetting = ts; // 構造体のクローン
                        tsWithSensorSetting.UsedSensors = Utility.MakeFlags(sensorIdx);
                        if (tsWithSensorSetting.IsNormalMethod)
                            compareBy1Activity1SensorPattern(tsWithSensorSetting);
                        else
                            compareBy1Activity1SensorPattern(tsWithSensorSetting, removingAttributeDict, physicalChar);
                    }
                    #endregion
                }
            });
        }

        // 指定した行動とセンサの組み合わせで比較する
        void compareBy1Activity1SensorPattern(TestSetting ts, 
            Dictionary<UserData, Dictionary<string, List<string>>> removingAttributesDict = null, 
            PhysicalChar? physicalChar = null) {

            var usedSensorString = Utility.UsedSensorString(ts.UsedSensors);
            var startMethodTime = DateTime.Now;
            MainWindow.CW.Print( "Working on {0} sensor {1} thread {2}", ErrorStatus.OK, ts.Activity, usedSensorString, Thread.CurrentThread.ManagedThreadId);
            // 行動とセンサの組み合わせでデータを制限
            var extractedInsDict = Utility.ExtractByActivityAndSensors(originalInstancesDict, selectedUsers, ts.Activity, ts.UsedSensors);

            if (ts.IsNormalMethod) { //相関を用いない
                if (ts.IsLabeled) { //手動ラベリング
                    normalLabeledMethod(ts, extractedInsDict);
                } else { // 行動認識した結果を用いる
                    normalRecognisedMethod(ts, extractedInsDict);
                }
            } else { // 相関を用いる
                if (ts.IsLabeled) { //手動ラベリング
                    correlationLabeledMethod(ts, extractedInsDict, removingAttributesDict, physicalChar.Value);
                } else {
                    throw new NotImplementedException();
                }
            }
            MainWindow.CW.Print("Finished {0} sensor {1} calc time : {2}", ErrorStatus.OK, ts.Activity, usedSensorString, (DateTime.Now - startMethodTime));
        }

        void normalLabeledMethod(TestSetting ts, Dictionary<UserData, Instances> extractedInsDict) {
            var oneActiOneSensResult = new OneActivityOneSensorResult() { Setting = ts };
            if (ts.IsLogDensity) {
                #region isLogDensity
                var clusterDict = createClusterDict(extractedInsDict, oneActiOneSensResult, "labeled");
                foreach (var testUser in selectedUsers) {
                    // testClustererに、手動でラベリングしたデータを用いる
                    var testClusterer = clusterDict[testUser];
                    compareByLogDensity(ref oneActiOneSensResult, testUser, testClusterer, extractedInsDict);
                }
                #endregion
            } else { // Cauchy Schwarz
                #region Cauchy Schwarz
                // GMM辞書の用意
                var gmmDict = createGMMDict(extractedInsDict, "[LABELED]");
                foreach (var testUser in selectedUsers) {
                    var testGmm = gmmDict[testUser];
                    compareByCauchySchwarz(ref oneActiOneSensResult, testUser, testGmm, gmmDict);
                }
                #endregion
            }
            // 結果の出力
            oneActiOneSensResult.Export();
        }

        void normalRecognisedMethod(TestSetting ts, Dictionary<UserData, Instances> extractedInsDict) {
            var recognisedExtractedInsDict = Utility.ExtractByActivityAndSensors(recognisedInstancesDict, selectedUsers, ts.Activity, ts.UsedSensors);
            if (ts.IsLogDensity) {
                ConcurrentDictionary<UserData, DensityBasedClusterer> labeledClusterDict = null;
                ConcurrentDictionary<UserData, DensityBasedClusterer> recognisedClusterDict = null;
                foreach (var testUser in selectedUsers) {
                    var oneActiOneSensResult = new OneActivityOneSensorResult() { Setting = ts, TestUser = testUser };
                    // Main Cluster辞書の用意
                    if(labeledClusterDict == null) labeledClusterDict = createClusterDict(extractedInsDict, oneActiOneSensResult , "labeled");
                    // RecognisedCluster辞書の用意
                    if(recognisedClusterDict == null) recognisedClusterDict = createClusterDict(recognisedExtractedInsDict, oneActiOneSensResult, "recognised");
                    var recognisedTestCluster = recognisedClusterDict[testUser];
                    var recognisedTestInstances = recognisedExtractedInsDict[testUser];
                    compareByLogDensity_Recognised(ref oneActiOneSensResult, testUser, recognisedTestCluster, recognisedTestInstances, labeledClusterDict, extractedInsDict);
                    oneActiOneSensResult.Export();
                }
            } else { // cauchy schwarz
                // Main GMM辞書の用意
                var mainGmmDict = createGMMDict(extractedInsDict, "[LABELED]");
                // Recognised GMM辞書の用意
                var recognisedGmmDict = createGMMDict(recognisedExtractedInsDict, "[RECOGNISED]");
                foreach (var testUser in selectedUsers) {
                    var oneActiOneSensResult = new OneActivityOneSensorResult() { Setting = ts, TestUser = testUser };
                    var recognisedGmm = recognisedGmmDict[testUser];
                    compareByCauchySchwarz_Recognised(ref oneActiOneSensResult, testUser, recognisedGmm, mainGmmDict);
                    oneActiOneSensResult.Export();
                }
            }
        }

        ConcurrentDictionary<UserData, DensityBasedClusterer> createClusterDict(Dictionary<UserData, Instances> userInstancesDict, 
            OneActivityOneSensorResult  oneActiOneSensResult, string fileAppendStr = "") {
            var beforeClusterBuild = DateTime.Now;
            var clusterDict = new ConcurrentDictionary<UserData, DensityBasedClusterer>();
            Parallel.ForEach(selectedUsers, user => {
                var modelFilePath = Path.Combine(oneActiOneSensResult.GetExportRootPath(), MODEL_DIR, string.Format("{0}_{1}_{2}", fileAppendStr, user.ID, oneActiOneSensResult.GetFileName(MODEL_EXT)));
                var clusterer = File.Exists(modelFilePath) ?
                    WekaClusterer.LoadSavedClusterer(modelFilePath, WekaConstantParams.WekaClusterers.EM, null, EM_CLUSTER_NUM) :
                    WekaClusterer.BuildClusterer(userInstancesDict[user], WekaConstantParams.WekaClusterers.EM, null, EM_CLUSTER_NUM, true, modelFilePath);
                clusterDict.TryAdd(user, (DensityBasedClusterer)clusterer);
            });
            MainWindow.CW.Print("{0} Build cluster time {1}", ErrorStatus.OK, fileAppendStr,  (DateTime.Now - beforeClusterBuild));
            return clusterDict;
        }

        ConcurrentDictionary<UserData, GaussianMixtureModel> createGMMDict(Dictionary<UserData, Instances> userInstancesDict, string message = "") {
            var beforeGmmBuild = DateTime.Now;
            var gmmDict = new ConcurrentDictionary<UserData, GaussianMixtureModel>();
            Parallel.ForEach(selectedUsers, user => {
                var gmm = new GaussianMixtureModel();
                gmm.setOptions(Utils.splitOptions(CLUSTERING_OPTION));
                gmm.buildClusterer(userInstancesDict[user]);
                gmmDict.TryAdd(user, gmm);
            });
            MainWindow.CW.Print("{0} Build gmm time {1}", ErrorStatus.OK, message, (DateTime.Now - beforeGmmBuild));
            return gmmDict;
        }

        void correlationLabeledMethod(TestSetting ts, Dictionary<UserData, Instances> extractedInsDict,
            Dictionary<UserData, Dictionary<string, List<string>>> removingAttributesDict, PhysicalChar physicalChar) {
                if (ts.IsLogDensity) {
                    // 相関の値が低かった成分の削除
                    //if (removingAttributesDict != null)
                    //    WekaUsefulMethods.ReduceAttributes(testInstances, removingAttributesDict[testUser][activityStr].ToArray());
                    throw new NotImplementedException();
                } else {
                    foreach (var testUser in selectedUsers) {
                        var oneActiOneSensResult = new OneActivityOneSensorResult() {
                            Setting = ts,
                            TestUser = testUser,
                            PhysicalChar = physicalChar
                        };

                        #region test user 用の GMM辞書の用意
                        var beforeGmmBuild = DateTime.Now;
                        // 各ユーザが使うべきインスタンスが決まったので、各自のgmmを構成してしまう
                        var gmmDict = new ConcurrentDictionary<UserData, GaussianMixtureModel>();
                        var removingAttributes = removingAttributesDict[testUser][ts.Activity.ToString()].ToArray();
                        Parallel.ForEach(selectedUsers, user => {
                            // instancesのコピー
                            var instances = new Instances(extractedInsDict[user]);
                            WekaUsefulMethods.ReduceAttributes(instances, removingAttributes);

                            // もし 0011のセンサの組み合わせで、0001しかデータがなかった場合、0011の結果は0001と同じになる
                            // GMMをbuildするのに時間がかかるため、0011は省略する。それを行う箇所
                            #region
                            var needBuild = true;
                            var usingSensorNum = ts.UsedSensors.Count(flg => flg);
                            // センサが一つの場合は無条件でビルド
                            if (usingSensorNum >= 2) {
                                var exportPath = oneActiOneSensResult.GetExportRootPath();
                                var exportedCsvFileNames = Directory.GetFiles(exportPath, "*.csv").Select(path => Path.GetFileNameWithoutExtension(path));
                                // 出力されたファイルが何個あるのかを調べる
                                var oneSensorExportedFileNum = 0;
                                for (int i = 0; i < 4; i++) {
                                    if (ts.UsedSensors[i]) {
                                        var flg = Utility.MakeFlags((int)Math.Pow(2, (3 - i)));
                                        // たとえば 0001を含むファイルが存在すると、anyはtrueを返す
                                        if (exportedCsvFileNames.Any(fname => fname.Contains(Utility.UsedSensorString(flg))))
                                            oneSensorExportedFileNum++;
                                    }
                                }
                                if (oneSensorExportedFileNum <= 1) needBuild = false;
                                else if (usingSensorNum == 4 && oneSensorExportedFileNum == 2) { // 1111の場合, 0011, 1100だったら、計算の必要がない
                                    if (exportedCsvFileNames.Any(fname => fname.Contains(Utility.UsedSensorString(Utility.MakeFlags(3))))
                                        || exportedCsvFileNames.Any(fname => fname.Contains(Utility.UsedSensorString(Utility.MakeFlags(12)))))
                                        needBuild = false;
                                }
                            }
                            #endregion

                            GaussianMixtureModel gmm = null;
                            // 相関で、特徴ベクトルの成分がすべて消されていないかを確認
                            if (instances.numAttributes() > 0 && needBuild) {
                                gmm = new GaussianMixtureModel();
                                gmm.setOptions(Utils.splitOptions(CLUSTERING_OPTION));
                                gmm.buildClusterer(instances);
                            }
                            // Attributeがない場合はnullを入れて、後に処理する
                            gmmDict.TryAdd(user, gmm);
                        });
                        MainWindow.CW.Print("User {0} Build gmm time {1}", ErrorStatus.OK, testUser.ID, (DateTime.Now - beforeGmmBuild));
                        #endregion

                        var testGmm = gmmDict[testUser];
                        if (testGmm != null)
                            compareByCauchySchwarz(ref oneActiOneSensResult, testUser, gmmDict[testUser], gmmDict);
                        oneActiOneSensResult.Export();
                    }
                }
        }


        // logdensityを用いた類似度取得方法
        void compareByLogDensity(ref OneActivityOneSensorResult oneActiOneSensResult, UserData testUser,
            DensityBasedClusterer testUserClusterer, Dictionary<UserData, Instances> comparingInstancesDict) {
            // 一人のユーザに対する結果を格納する辞書
            var comparedUserResultList = new List<Tuple<UserData, double>>();
            // 各インスタンス毎に比較を行う
            foreach (var comparingUser in selectedUsers) {
                var comparingInsts = comparingInstancesDict[comparingUser];
                comparedUserResultList.Add(new Tuple<UserData, double>(comparingUser, calcAverageLogDensity(testUserClusterer, comparingInsts)));
            }
            oneActiOneSensResult.ResultDict.Add(testUser, comparedUserResultList);
        }
        
        void compareByLogDensity_Recognised(ref OneActivityOneSensorResult oneActiOneSensResult, UserData testUser,
            DensityBasedClusterer recognisedTestClusterer, Instances recognisedInstances, 
            ConcurrentDictionary<UserData, DensityBasedClusterer> labeledClusterDict, Dictionary<UserData, Instances> comparingInstancesDict) {
            var labeledExportedCsvPath = oneActiOneSensResult.GetExportedCsvPathIfLabeled();
            if (File.Exists(labeledExportedCsvPath)==false) throw new Exception("There should be labeled results files!!");
            // ラベリングされた結果を読みだす
            oneActiOneSensResult.ResultDict = LoadCsvSimilarityTable(labeledExportedCsvPath, selectedUsers);
            
            #region ラベリングした場合の結果を行動認識した結果で上書きする
            // testUser -> comparingUsers
            // 一人のユーザに対する結果を格納する辞書
            var comparedUserResultList = new List<Tuple<UserData, double>>(); 
            foreach (var comparingUser in selectedUsers) {
                var comparingInsts = comparingInstancesDict[comparingUser];
                comparedUserResultList.Add(new Tuple<UserData, double>(comparingUser, calcAverageLogDensity(recognisedTestClusterer, comparingInsts)));
            }
            oneActiOneSensResult.ResultDict[testUser] = comparedUserResultList;
            // comparingUsers -> testUser
            var testUserIdx = selectedUsers.IndexOf(testUser);
            foreach (var comparingUser in selectedUsers) {
                if (comparingUser == testUser) continue;
                if(oneActiOneSensResult.ResultDict[comparingUser][testUserIdx].Item1 != testUser) throw new Exception("Putting data to different user !!");
                oneActiOneSensResult.ResultDict[comparingUser][testUserIdx] 
                    = new Tuple<UserData,double>(testUser, calcAverageLogDensity(labeledClusterDict[comparingUser], recognisedInstances));
            }
            #endregion
        }

        double calcAverageLogDensity(DensityBasedClusterer testCluster, Instances comparingInstances) {
            double aveLogDensity = 0.0;
            var numOfInsts = comparingInstances.numInstances();
            for (int instIdx = 0; instIdx < numOfInsts; instIdx++) {
                aveLogDensity += WekaClusterer.LogDensity(testCluster, comparingInstances.instance(instIdx));
                //aveLogDensity += WekaClusterer.NormalizedLogDensity((weka.clusterers.DensityBasedClusterer)testUserClusterer, comparingInsts.instance(i));
            }
            // devide by number of instances
            aveLogDensity /= numOfInsts;
            return aveLogDensity;
        }


        // caushySchwarzDistanceを用いた類似度計算
        void compareByCauchySchwarz(ref OneActivityOneSensorResult oneActiOneSensResult, UserData testUser, 
            GaussianMixtureModel testGmm, ConcurrentDictionary<UserData, GaussianMixtureModel> comparingGmmDict) {
            // 一人のユーザに対する結果を格納するリスト
            var comparedUserResultList = new List<Tuple<UserData, double>>();
            CauchySchwarzDivGMM csdGMM = null;
            int testUserIndex = selectedUsers.IndexOf(testUser);
            foreach (var comparer in selectedUsers.Select((u, i) => new { User = u, Idx = i })) {
                if (testUserIndex > comparer.Idx) {
                    // CauchySchwarzDivGMMは対称性を持つため、すでにある結果と同じになる
                    comparedUserResultList.Add(new Tuple<UserData, double>(comparer.User, oneActiOneSensResult.ResultDict[comparer.User][testUserIndex].Item2));
                } else if (testUserIndex == comparer.Idx) {
                    // 自分自身とは類似度が０
                    comparedUserResultList.Add(new Tuple<UserData, double>(testUser, 0));
                } else {
                    // 結果を計算する
                    csdGMM = new CauchySchwarzDivGMM(testGmm, comparingGmmDict[comparer.User]);
                    var result = csdGMM.GetDcsResult();
                    comparedUserResultList.Add(new Tuple<UserData, double>(comparer.User, result));
                }
            }
            oneActiOneSensResult.ResultDict.Add(testUser, comparedUserResultList);
        }
        
        void compareByCauchySchwarz_Recognised(ref OneActivityOneSensorResult oneActiOneSensResult, UserData testUser,
            GaussianMixtureModel recognisedGmm, ConcurrentDictionary<UserData, GaussianMixtureModel> comparingGmmDict) {
            var labeledExportedCsvPath = oneActiOneSensResult.GetExportedCsvPathIfLabeled();
            if (File.Exists(labeledExportedCsvPath) == false) throw new Exception("There should be labeled results files!!");
            // ラベリングされた結果を読みだす
            oneActiOneSensResult.ResultDict = LoadCsvSimilarityTable(labeledExportedCsvPath, selectedUsers);
            
            #region ラベリングした場合の結果を行動認識した結果で上書きする
            CauchySchwarzDivGMM csdGMM = null;
            int testUserIdx = selectedUsers.IndexOf(testUser);
            foreach (var compareTpl in selectedUsers.Select((u,i) => new {User = u, Idx = i})) {
                if(compareTpl.User == testUser) continue; // もう0が入っているためパス
                // 結果を計算する
                csdGMM = new CauchySchwarzDivGMM(recognisedGmm, comparingGmmDict[compareTpl.User]);
                var result = csdGMM.GetDcsResult();
                // testUser -> compareUser
                oneActiOneSensResult.ResultDict[testUser][compareTpl.Idx] = new Tuple<UserData,double>(compareTpl.User, result);
                // compareUser -> testUser 
                oneActiOneSensResult.ResultDict[compareTpl.User][testUserIdx] = new Tuple<UserData,double>(testUser, result);
            }
            #endregion
        }

        // あるテストユーザにおいて、caushySchwarzDistanceを用いた類似度取得(相関を用いて、特徴ベクトルを削減した場合)
        private void compareByCauchySchwarz(ref OneActivityOneSensorResult oneActiOneSensResult, 
            ConcurrentDictionary<UserData, ConcurrentDictionary<UserData, GaussianMixtureModel>> gmmDict, UserData testUser) {
            // 相関で削除された[行動, センサの組み合わせ]であった
            if (gmmDict[testUser][testUser] == null) return;

            CauchySchwarzDivGMM csdGMM = null;
            var resultDict = new Dictionary<UserData, List<Tuple<UserData, double>>>();
            foreach (var user in selectedUsers) resultDict.Add(user, new List<Tuple<UserData, double>>());
            for (int user1Index = 0; user1Index < selectedUsers.Count; user1Index++) {
                var user1 = selectedUsers[user1Index];
                for (int user2Index = 0; user2Index < selectedUsers.Count; user2Index++) {
                    var user2 = selectedUsers[user2Index];
                    if (user1Index == user2Index) {
                        // 自分自身とは類似度が０
                        resultDict[user1].Add(new Tuple<UserData, double>(user1, 0));
                    } else if (user1Index < user2Index) {
                        csdGMM = new CauchySchwarzDivGMM(gmmDict[testUser][user1], gmmDict[testUser][user2]);
                        var result = csdGMM.GetDcsResult();
                        resultDict[user1].Add(new Tuple<UserData, double>(user2, result));
                    } else if (user1Index > user2Index) {
                        // 計算済みであるため、resultDictからデータを取得する
                        resultDict[user1].Add(new Tuple<UserData, double>(user2, resultDict[user2][user1Index].Item2));
                    }
                }
            }
            oneActiOneSensResult.ResultDict = resultDict;
        }



        public static Dictionary<UserData, List<Tuple<UserData, double>>> LoadCsvSimilarityTable(string csvExportedSimilarityTablePath, List<UserData> selectedUsers) {
            // 手動でラベリングした結果を取り込む
            var resultDict = new Dictionary<UserData, List<Tuple<UserData, double>>>();
            using (var sr = new StreamReader(csvExportedSimilarityTablePath)) {
                while (sr.Peek() > 0) {
                    // 0:userData, 1: 0000のデータ, 2:0001のデータ ...
                    var splitline = sr.ReadLine().Split(',');
                    var userIDStr = splitline[0].Substring(0, 4);
                    var user = MainWindow.UserIDDict[userIDStr];
                    resultDict[user] = splitline.Skip(1).Select((strVal, idx) => {
                        double dblVal = double.NaN;
                        double.TryParse(strVal, out dblVal);
                        return new Tuple<UserData, double>(selectedUsers[idx], dblVal);
                    }).ToList();
                }
            }
            return resultDict;
        }


        #region TestSetting
        [Serializable]
        struct TestSetting {
            public string ProcessDirPath { get; set; }
            public bool IsNormalMethod { get; set; }
            public bool IsLogDensity { get; set; }
            public bool IsLabeled { get; set; }
            public Activity Activity { get; set; }
            public bool[] UsedSensors { get; set; }
        }
        #endregion

        #region OneActivityResult ある行動、センサの組み合わせにおける結果

        [Serializable]
        class OneActivityOneSensorResult {
            public TestSetting Setting { get; set; }
            [NonSerialized]
            Dictionary<UserData, List<Tuple<UserData, double>>> resultDict;
            public Dictionary<UserData, List<Tuple<UserData, double>>> ResultDict { get { return resultDict; } set { resultDict = value; } }
            Dictionary<string, List<Tuple<string, double>>> serializableResultDict;
            #region ユーザごとに類似度テーブルが異なる場合に用いる変数
            string testUserId = "";
            [NonSerialized]
            UserData testUser = null;
            public UserData TestUser { get { return testUser; } set { this.testUser = value; } }
            public PhysicalChar PhysicalChar { get; set; }
            #endregion
            [NonSerialized]
            string exportRootPath = null;

            // 引数なしコンストラクタ serializeでは必要
            public OneActivityOneSensorResult() {
                this.ResultDict = new Dictionary<UserData, List<Tuple<UserData, double>>>();
            }

            public string GetExportRootPath(){
                if(exportRootPath == null){
                    var methodName = Setting.IsNormalMethod ? "Normal" : "Correlation";
                    var labelOrRecognised = Setting.IsLabeled ? "Labeled" : "Recognised";
                    var simiralityMethod = Setting.IsLogDensity ? LOGDENSITY : CAUCHY_SCHWARZ;
                    exportRootPath = Path.Combine(Setting.ProcessDirPath, CALC_SIM_DIR, methodName, labelOrRecognised, simiralityMethod);
                    if (Setting.IsNormalMethod && Setting.IsLabeled) {
                        exportRootPath = Path.Combine(exportRootPath, Setting.Activity.ToString());
                    } else {
                        exportRootPath = Path.Combine(exportRootPath, testUser.ID, Setting.Activity.ToString());
                    }
                }
                return exportRootPath;
            }

            public string GetExportedCsvPathIfLabeled() {
                var methodName = Setting.IsNormalMethod ? "Normal" : "Correlation";
                var simiralityMethod = Setting.IsLogDensity ? LOGDENSITY : CAUCHY_SCHWARZ;
                var labeledExportRootPath = Path.Combine(Setting.ProcessDirPath, CALC_SIM_DIR, methodName, "Labeled", simiralityMethod);
                if (Setting.IsNormalMethod) {
                    labeledExportRootPath = Path.Combine(labeledExportRootPath, Setting.Activity.ToString());
                } else {
                    labeledExportRootPath = Path.Combine(labeledExportRootPath, testUser.ID, Setting.Activity.ToString());
                }
                return Path.Combine(labeledExportRootPath, GetFileName(".csv"));
            }

            //ある行動のあるセンサの組み合わせで比較した結果を.csvとして出力する
            public void Export() {
                if (this.ResultDict == null || this.ResultDict.Count == 0) return;
                var exportPath = GetExportRootPath();
                Directory.CreateDirectory(exportPath);
                var sortedUsers = this.ResultDict.Keys.ToList();
                sortedUsers.Sort(new UserDataComparer());
                using (var sw = new StreamWriter(Path.Combine(exportPath, GetFileName(CSV_EXT)))) {
                    foreach (var user in sortedUsers)
                        sw.WriteLine(user + "," + string.Join(",", this.ResultDict[user].Select(t => t.Item2)));
                }
                // Dictionaryはxml serializeできないため、binary serialize
                // しかし、UserDataもserialize不可なため、userIDを保存する
                serializableResultDict = new Dictionary<string, List<Tuple<string, double>>>();
                foreach (var kv in ResultDict) {
                    serializableResultDict.Add(kv.Key.ID, kv.Value.Select(tpl => new Tuple<string, double>(tpl.Item1.ID, tpl.Item2)).ToList());
                }
                if (testUser != null) testUserId = testUser.ID;
                using (var fs = new FileStream(Path.Combine(exportPath, GetFileName(BIN_EXT)), FileMode.Create, FileAccess.Write, FileShare.None)) {
                    var formatter = new BinaryFormatter();
                    formatter.Serialize(fs, this);
                }
            }
            public static OneActivityOneSensorResult Iｍport(string binFilePath) {
                if (File.Exists(binFilePath) == false) return null;
                OneActivityOneSensorResult importedData = null;
                using (var fs = new FileStream(binFilePath, FileMode.Open, FileAccess.Read, FileShare.Read)) {
                    var formatter = new BinaryFormatter();
                    importedData = formatter.Deserialize(fs) as OneActivityOneSensorResult;
                }
                importedData.ResultDict = new Dictionary<UserData, List<Tuple<UserData, double>>>();
                foreach (var kv in importedData.serializableResultDict) {
                    importedData.ResultDict.Add(MainWindow.UserIDDict[kv.Key], 
                        kv.Value.Select(tpl => new Tuple<UserData, double>(MainWindow.UserIDDict[tpl.Item1], tpl.Item2)).ToList());
                }
                if(importedData.testUserId != "")
                    importedData.TestUser = MainWindow.UserIDDict[importedData.testUserId];
                return importedData;
            }

            // 行動、センサ、類似度測定に何を使ったのかをファイル名とする
            public string GetFileName(string extension){
                return makeFileName(this.Setting.Activity, this.Setting.UsedSensors, this.Setting.IsLogDensity, extension);
            }
            string makeFileName(Activity activity, bool[] usedSensors, bool isLogDensity, string extension) {
                var clusterInfo = isLogDensity ? "_EMCLUSTER_" + EM_CLUSTER_NUM : "_GMMCLUSTER_" + GMM_CLUSTERING_NUM;
                return new StringBuilder(activity.ToString()).Append("_lrwt_").Append(Utility.UsedSensorString(usedSensors))
                                    .Append("_ARFF_").Append(Utility.READ_ARFF_NUMBER).Append(clusterInfo).Append(extension).ToString();
            }

        }
        #endregion


        // 行動認識した結果を格納しているフォルダを指定する
        private void recognisedDataRadioButton_Checked(object sender, RoutedEventArgs e) {
            if (Directory.Exists(recognisedDataRootPathTextBlock.Text)) return;
            var recognisedDataRootPath = Utility.SelectFolderPath(dman.WekaWorkingRoot(), "Choose Recognised Arff Root Path");
            if (recognisedDataRootPath == null) return;
            recognisedDataRootPathTextBlock.Text = recognisedDataRootPath;
            Properties.Settings.Default.CalcSimiralityOfUsers_RecognisedDataPath = recognisedDataRootPath;
            Properties.Settings.Default.Save();
        }
        // 行動認識した結果とラベリングされたarffをマージさせる
        private void processRecognisedDataButton_Click(object sender, RoutedEventArgs e) {
            var recognisedDataRootPath = recognisedDataRootPathTextBlock.Text;
            if (Directory.Exists(recognisedDataRootPath) == false) return;
            var featureArffPath = Path.Combine(dman.cdman.rootpath, "arff");
            Task.Factory.StartNew(() => {
                foreach (var userDirPath in Directory.GetDirectories(recognisedDataRootPath)) {
                    var userIDStr = Path.GetFileName(userDirPath);
                    var testUser = MainWindow.UserIDDict[userIDStr];
                    // 行動認識の結果から新しいarffファイルを作成
                    var recognisedSessionFilePaths = Directory.GetFiles(Path.Combine(userDirPath, "results")).ToList();
                    recognisedSessionFilePaths.Sort();
                    // 特徴ベクトルのファイルも並び替えを行う
                    var featureSessionFilePaths = Directory.GetFiles(Path.Combine(featureArffPath, userIDStr), "*.arff").ToList();
                    featureSessionFilePaths.Sort();
                    for (int sessionIdx = 0; sessionIdx < Utility.READ_ARFF_NUMBER; sessionIdx++) {
                        var oneSessionInstances = WekaUsefulMethods.LoadArffFile(featureSessionFilePaths[sessionIdx]);
                        var classIdx = oneSessionInstances.numAttributes() - 1;
                        oneSessionInstances.setClassIndex(classIdx);
                        using (var sr = new StreamReader(recognisedSessionFilePaths[sessionIdx])) {
                            int instanceIdx = 0;
                            while (sr.Peek() > 0) {
                                var posibilityList = sr.ReadLine().Split(',').Skip(1);
                                // 最大の確率を持った行動を取得する
                                var maxPosIdx = posibilityList.Select((strVal, idx) => {
                                    double val = 0; double.TryParse(strVal, out val); return new { I = idx, V = val };
                                }).Aggregate((l, r) => (l.V > r.V ? l : r)).I;
                                oneSessionInstances.instance(instanceIdx).setClassValue(RECOGNISED_ACTIVITIES[maxPosIdx]);
                                instanceIdx++;
                            }
                        }
                        WekaUsefulMethods.SaveArff(oneSessionInstances, Path.Combine(userDirPath, RECOGNISED_ARFF, sessionIdx + ".arff"));
                    }
                    MainWindow.CW.Print("Finish Creating Recognised Arff for " + userIDStr);
                }
            });
        }

        #endregion

        PhysicalChar getCheckedUserPhysicalChar() {
            if (estimateAgeCorrelationRadioButton.IsChecked.Value) return PhysicalChar.age;
            if (estimateHeightCorrelationRadioButton.IsChecked.Value) return PhysicalChar.height;
            if (estimateWeightCorrelationRadioButton.IsChecked.Value) return PhysicalChar.weight;
            return PhysicalChar.height;
        }

        Dictionary<string, int> attributeDictionary;
        int getAttributeIndex(string attributeName) {
            if (attributeDictionary == null) {
                attributeDictionary = new Dictionary<string, int>();
                for (int idx = 0; idx < ATTRIBUTE_LIST.Length; idx++) attributeDictionary.Add(ATTRIBUTE_LIST[idx], idx);
            }
            return attributeDictionary[attributeName];
        }

        // GUI events

        #region listbox events

        private void testUser_listBox_SelectedIndexChanged(object sender, System.EventArgs e) {
            //if (aveLogDensity_listBox.Items.Count != 0) aveLogDensity_listBox.Items.Clear();
            //if (comparedUser_listBox.Items.Count != 0) comparedUser_listBox.Items.Clear();

            //var activity = activity_comboBox.SelectedItem as string;
            //// 選択したユーザーの結果を表示する
            //var selectedUser = testUser_listBox.SelectedItem as UserData;
            //// ユーザーの情報を表示する
            //testUserData_textBox.Text = selectedUser.ShowUserData();
            //var selectedUserResultList = resultDictionary[selectedUser];
            //foreach (var dataTuple in selectedUserResultList)
            //{
            //    aveLogDensity_listBox.Items.Add(dataTuple.Item1.ToString());
            //    comparedUser_listBox.Items.Add(dataTuple.Item2);
            //}
        }
        //private void comparedUser_listBox_SelectedIndexChanged(object sender, System.EventArgs e)
        //{
        //    var selectedComparedUser = aveLogDensity_listBox.SelectedItem as UserData;
        //    // ユーザーの情報を表示する
        //    comparedUserData_textBox.Text = selectedComparedUser.ShowUserData();
        //}

        #endregion

    }
}
